# BBB@Scale Compared to Greenlight and Scalelite

Disclaimer: the purpose of the following table is to provide a quick high-level feature-based overview of BBB@Scale compared to Greenlight and Scalelite. 
We compiled the table to the best of our knowledge and in best faith. If we misinterpreted the capabilites of Greenlight and/or Scalelite, please reach out so we can fix our errors.



|                                                                | Greenlight         | Scalelite          | BBB@Scale          |
|----------------------------------------------------------------|--------------------|--------------------|--------------------|
| **Greenlight features**                                        |                    |                    |                    |
| Integrate Moodle with a singel BBB servers                     |:white_check_mark:  |:x:                 | :x:[^2]            |
| Personalized Rooms (Rooms are grouped under users)             |                    |                    |                    |
|   -> Home room for individual users                            |:white_check_mark:  |:x:                 | :white_check_mark: |
|   -> Users can create their own rooms                          |:white_check_mark:  |:x:                 | :white_check_mark:[^9]            |
|   -> Share personalized room with other users                  |:white_check_mark:  |:x:                 | :white_check_mark:[^9]            |
| Authentication with Google/Office365                           |:white_check_mark:  |[^6]                | :white_check_mark:[^11]           |
| Authentication via LDAP                                        |:white_check_mark:  |[^6]                |:white_check_mark:  |
| Assign roles to users                                          |:white_check_mark:  |[^6]                |:white_check_mark:  |
| Custom Design / Branding                                       |:white_check_mark:  |[^6]                | :x:[^12]           |
| Basic configuration options of each room[^10]                  |:white_check_mark:  |[^6]                |:white_check_mark:  |
| Account Settings & User Preferences                            |:white_check_mark:  |[^6]                | :x:                |
| Allow users to wait for your room to start, then automatically join when it does |:white_check_mark:|[^6]| :white_check_mark: |
| User & Room Management                                         |:white_check_mark:  |:x:                 | :white_check_mark: |
| Invite others to your room using a simple URL                  |:white_check_mark:  | [^6]               | :white_check_mark: |
| Create and View recordings of rooms and share them with others.|:white_check_mark:  | :x:                | :white_check_mark: |
| Send Recording via E-Mail                                      |:white_check_mark:  | :x:                |                    |
| Recordings can be private or public                            |:white_check_mark:  | :x:                |                    |
| Banning Users (prevent further sign ups with same e-mail)      |:white_check_mark:  | :x:                | :x:                |
|                                                                |                    |                    |                    |
| **Scalelite features**                                         |                    |                    |                    |
| Manage/schedule rooms over a pool of BBB servers               |:x:                 |:white_check_mark:  |:white_check_mark:  |
| Dynamic load balancing                                         |:x:                 |:white_check_mark:  |:white_check_mark:  |
| Simple BBB recordings management over pool of BBB servers[^4]  |:x:                 |:white_check_mark:  |:white_check_mark:/:x:[^3] | 
| Integrate Moodle with a pool of BBB servers[^1]                |:white_check_mark:  |:white_check_mark:  |:x:[^2]             |
|                                                                |                    |                    |                    |
| **BBB@Scale unique features**                                  |                    |                    |                    |
| Represent school's physical rooms (e.g. hybrid lecture virtual + presence) |:x:                 |:x:                 |:white_check_mark:  |
| Role-based privileges. BBB room usage based on LDAP groups (e.g. teachers are always moderators in rooms; students are viewers) |:x:                 |:x:                 |:white_check_mark:  |
| Complex BBB recordings management over pool of BBB servers[^5] |:x:                 |:x:                 |:white_check_mark:/:x:[^3] |
| Stats and monitoring. See the utilization of BBB servers       |:x:                 |:x:                 |:white_check_mark:  |
| Detailed configuration options for each room[^10]              |:x:                 |:x:                 |:white_check_mark:  |
| Tenant concept: create server pools and rooms per tenant       |:x:                 |:x:                 |:white_check_mark:  |
| Create room types (templates for room configuration)           |:x:                 |:x:                 |:white_check_mark:  |
| Room event planning (e.g. time & date of lectures)             |:x:                 |:x:                 |:white_check_mark:  |
| Import and Export room configuration                           |:x:                 |:x:                 |:white_check_mark:  |
| Automatic conversion of recordings to mp4                      |:x:                 |:x:                 |:white_check_mark:/:x:[^3]  |
| Automatic publishing of recordings (as mp4) to OpenCast        |:x:                 |:x:                 |:white_check_mark:/:x:[^3]  |
| Intregration of KeyCloak as IAM integration layer              |:x:                 |:x:                 |:x:[^11]  |
| Build in support chat to give real-time support to useres      |:x:                 |:x:                 |:white_check_mark: |


[^1]: There is a Moodle plugin, that allows to directly integrate Moodle with BigBlueButton. Work in progress [Link](https://moodle.org/plugins/mod_bigbluebuttonbn)
[^2]: High priority issue for one of the next releases #19
[^3]: Feature exists but is not fully documented yet
[^4]: Simple, because a single BBB server serves as both recording and playback machine. See the complex feature for more details. **TODO: double check if this is still the case** 
[^5]: It is possible to have dedicated playback and rendering BBB servers that are not used for normal room hosting and recording. After a recording is made, recordings are migrated to a pool of rendering and playback BBB servers for users' consumption.
[^6]: Scalelite is a load balancer and does not provide a frontend - characteristic is not applicable
[^8]: Issues for implementation of this issue: #38 #35
[^9]: Issues for implementation of this issue: #36 #37
[^10]: BBB@Scale appears to offer more options to configure rooms, thus this point has been split in basic and detailed
[^11]: Work in progress: #57
[^12]: We are currently creating a dark theme also creating documentation on customization UI #42
