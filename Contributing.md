# Contributing

If you have just got time and you want to contribute: haver a look at the tags [Easy](https://gitlab.com/bbbatscale/bbbatscale/-/issues?label_name%5B%5D=Easy), [Cleaning](https://gitlab.com/bbbatscale/bbbatscale/-/issues?label_name%5B%5D=Cleaning), [Documentation](https://gitlab.com/bbbatscale/bbbatscale/-/issues?label_name%5B%5D=Documentation) or [Feature](https://gitlab.com/bbbatscale/bbbatscale/-/issues?label_name%5B%5D=Feature).


## Working with issues
If you want to contribute something this is great! 
Before starting to write code or do something else: please check, if an issue to that topic already exists please do not create a new one.
And if an person is already assigned to an issue you wanna do: check up on that person has already started or has some tips for you.


## Commits

We follow the [angular commit guide](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-guidelines).
That means commit follow the pattern 
```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```
~~At some point in the future commits should be used to create changelog. You can watch #43 to see the process. ~~
That means, if you want that your new feature, bugfix, performance improvement or something else shows up correctly in the change-notes you need to follow the [commit guidelines from Angular](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#commit-message-header).
Then this will trigger automatically the creation of a new patch, minor or major release as defined in [Semantic Versioning](https://semver.org/).
If you are interessted in the technical background of the rules used to determine which "level" of release is choosen have a look at [semantic-release](https://github.com/semantic-release/commit-analyzer#releaserules) and [default-release-rules.js](https://github.com/semantic-release/commit-analyzer/blob/master/lib/default-release-rules.js).
