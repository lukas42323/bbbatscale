from django.conf import settings

from core.models import GeneralParameter


def general_parameter(request):
    return {'general_parameter': GeneralParameter.load()}


def oidc(request):
    return {'oidc_enabled': settings.OIDC_ENABLED}
