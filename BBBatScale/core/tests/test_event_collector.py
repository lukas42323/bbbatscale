import os
from datetime import timezone, datetime
from unittest.mock import patch

from django.conf import settings
from django.test import TestCase
from freezegun import freeze_time

from core.event_collectors.base import EventCollectorContext
from core.models import RoomEvent, Tenant, Server, Room, get_default_room_config


class EventCollectionTestCase(TestCase):

    def setUp(self):
        self.qis_ical_collector_test_ical_path = "file://" + os.path.dirname(
            os.path.realpath(__file__)) + "/test_data/QISICalCollector_ical-testdata.ics"

        def change_line_endings_to_windows(full_file_path):
            load_file_path = full_file_path[7:]
            temp_file_path = load_file_path[:-4] + "_temp" + load_file_path[-4:]

            # replacement strings
            WINDOWS_LINE_ENDING = '\r\n'
            UNIX_LINE_ENDING = '\n'

            with open(load_file_path, 'r') as open_file:
                content = open_file.read()

            content = content.replace(UNIX_LINE_ENDING, WINDOWS_LINE_ENDING)

            with open(temp_file_path, 'w') as open_file:
                open_file.write(content)

            temp_file_path = full_file_path[:7] + temp_file_path

            return temp_file_path

        self.qis_ical_collector_test_ical_path = change_line_endings_to_windows(self.qis_ical_collector_test_ical_path)

        self.fbi = Tenant.objects.create(
            name="Fachbereich Informatik",
        )

        self.bbb_server = Server.objects.create(
            tenant=self.fbi,
            dns="example.org",
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_d14_0303_qis = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server,
            name="D14/03.03",
            participant_count=10,
            videostream_count=5,
            event_collection_strategy="QISICalCollector",
            event_collection_parameters='{"qis_url": "qis.example.org", '
                                        '"qis_id": "118", '
                                        '"qis_encoding": "UTF-8"}',
            config=get_default_room_config()
        )

        file_path = "file://" + os.path.dirname(
            os.path.realpath(__file__)) + "/test_data/SimpleICalCollector_testdata.ics"
        self.room_d14_0204_ical = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server,
            name="D14/02.04",
            participant_count=10,
            videostream_count=5,
            event_collection_strategy="SimpleICalCollector",
            event_collection_parameters='{"iCal_url": "' + file_path + '", "iCal_encoding": "UTF-8"}',
            config=get_default_room_config()
        )
        settings.EVENT_COLLECTION_SYNC_SYNC_HOURS = 24

        self.incomplete_room = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server,
            name="D15/00.04",
            participant_count=10,
            videostream_count=5,
            config=get_default_room_config()
        )

    def tearDown(self):
        file_path = self.qis_ical_collector_test_ical_path[7:]
        if os.path.exists(file_path):
            os.remove(file_path)

    def test_strategy_not_set(self):
        with self.assertRaises(ModuleNotFoundError):
            EventCollectorContext(self.incomplete_room.event_collection_strategy)

    def test_parameters_not_set(self):
        self.incomplete_room.event_collection_strategy = "SimpleICalCollector"
        self.incomplete_room.save()

        context = EventCollectorContext(self.incomplete_room.event_collection_strategy)

        with self.assertRaises(TypeError):
            context.collect_events(self.incomplete_room.pk, self.incomplete_room.event_collection_parameters)

    def test_parameters_not_properly_formatted(self):
        self.incomplete_room.event_collection_strategy = "SimpleICalCollector"
        self.incomplete_room.save()

        context = EventCollectorContext(self.incomplete_room.event_collection_strategy)

        self.incomplete_room.event_collection_parameters = '{"broken": "true", "reason": "missing curly"'
        self.incomplete_room.save()

        with self.assertRaises(Exception):
            context.collect_events(self.incomplete_room.pk, self.incomplete_room.event_collection_parameters)

    def test_load_existing_strategy(self):
        context = EventCollectorContext("SimpleICalCollector")

        from core.event_collectors.SimpleICalCollector import SimpleICalCollector
        self.assertTrue(isinstance(context._strategy, SimpleICalCollector))

        context = EventCollectorContext("QISICalCollector")

        from core.event_collectors.QISICalCollector import QISICalCollector
        self.assertTrue(isinstance(context._strategy, QISICalCollector))

    def test_load_non_existing_strategy(self):
        with self.assertRaises(ModuleNotFoundError):
            EventCollectorContext("DoesNotExist")

    def test_simple_ical_collector_parameters_missing_(self):
        self.incomplete_room.event_collection_strategy = "SimpleICalCollector"
        self.incomplete_room.save()

        context = EventCollectorContext(self.incomplete_room.event_collection_strategy)

        self.incomplete_room.event_collection_parameters = '{"iCal_encoding": "UTF-8"}'
        self.incomplete_room.save()

        with self.assertRaises(Exception):
            context.collect_events(self.incomplete_room.pk, self.incomplete_room.event_collection_parameters)

    @freeze_time("2020-05-25 06:30:00", tz_offset=0)
    def test_simple_ical_collector_room_events_creation(self):

        context = EventCollectorContext(self.room_d14_0204_ical.event_collection_strategy)
        context.collect_events(self.room_d14_0204_ical.pk, self.room_d14_0204_ical.event_collection_parameters)

        room_events = RoomEvent.objects.filter(room=self.room_d14_0204_ical.pk)
        self.assertEqual(len(room_events), 4)

    @freeze_time("2020-05-23 00:00:00", tz_offset=0)
    def test_simple_ical_collector_room_events_creation_no_events_collected(self):

        context = EventCollectorContext(self.room_d14_0204_ical.event_collection_strategy)
        context.collect_events(self.room_d14_0204_ical.pk, self.room_d14_0204_ical.event_collection_parameters)

        room_events = RoomEvent.objects.filter(room=self.room_d14_0204_ical.pk)
        self.assertEqual(len(room_events), 0)

    def test_qis_ical_collector_parameters_missing_(self):
        self.incomplete_room.event_collection_strategy = "QISICalCollector"
        self.incomplete_room.save()

        context = EventCollectorContext(self.incomplete_room.event_collection_strategy)

        self.incomplete_room.event_collection_parameters = '{"qis_id": "118", "qis_encoding": "UTF-8"}'
        self.incomplete_room.save()

        with self.assertRaises(Exception):
            context.collect_events(self.incomplete_room.pk, self.incomplete_room.event_collection_parameters)

    @freeze_time("2020-05-25 06:30:00", tz_offset=0)
    @patch("core.event_collectors.QISICalCollector.QISICalCollector._extract_qis_ical_url")
    def test_qis_ical_collector_room_events_creation(self, mock_extract_function):
        mock_extract_function.return_value = self.qis_ical_collector_test_ical_path

        context = EventCollectorContext(self.room_d14_0303_qis.event_collection_strategy)

        context.collect_events(self.room_d14_0303_qis.pk, self.room_d14_0303_qis.event_collection_parameters)

        room_events = RoomEvent.objects.filter(room=self.room_d14_0303_qis.pk)
        self.assertEqual(len(room_events), 3)

    @freeze_time("2020-05-23 00:00:00", tz_offset=0)
    @patch("core.event_collectors.QISICalCollector.QISICalCollector._extract_qis_ical_url")
    def test_qis_ical_collector_room_events_creation_no_event_collected(self, mock_extract_function):
        mock_extract_function.return_value = self.qis_ical_collector_test_ical_path

        context = EventCollectorContext(self.room_d14_0303_qis.event_collection_strategy)

        context.collect_events(self.room_d14_0303_qis.pk, self.room_d14_0303_qis.event_collection_parameters)

        room_events = RoomEvent.objects.filter(room=self.room_d14_0303_qis.pk)
        self.assertEqual(len(room_events), 0)

    def test_event_collector_update_room_events_in_db(self):
        room = self.room_d14_0204_ical

        room_events_1 = {}
        room_events_1["1"] = RoomEvent(uid="1", room=room, name="Event 1",
                                       start=datetime(2020, 5, 25, 8, 30, tzinfo=timezone.utc),
                                       end=datetime(2020, 5, 25, 9, 0, tzinfo=timezone.utc))
        room_events_1["2"] = RoomEvent(uid="2", room=room, name="Event 2",
                                       start=datetime(2020, 5, 25, 10, 15, tzinfo=timezone.utc),
                                       end=datetime(2020, 5, 25, 11, 45, tzinfo=timezone.utc))
        room_events_1["3"] = RoomEvent(uid="3", room=room, name="Event 3",
                                       start=datetime(2020, 5, 25, 12, 0, tzinfo=timezone.utc),
                                       end=datetime(2020, 5, 25, 13, 30, tzinfo=timezone.utc))

        context = EventCollectorContext(room.event_collection_strategy)
        context._strategy._update_room_events_in_db(room_events_1, room.pk)

        room_events_1_from_db = list(RoomEvent.objects.filter(room=room.pk))

        self.assertListEqual(list(room_events_1.values()),
                             room_events_1_from_db,
                             "room_events_1 was not stored and retrieved from DB properly")

        # Special about this data set compared to room_events_1:
        # 1. room_event_1[1] was removed
        # 2. room_event_1[2] date has changed
        # 3. the time and name of room_event_1[3] has changed
        # 4. room_event_1[4] is new
        room_events_2 = {}
        room_events_2["2"] = RoomEvent(uid="2", room=room, name="Event 2",
                                       start=datetime(2020, 5, 25, 10, 15, tzinfo=timezone.utc),
                                       end=datetime(2020, 5, 25, 11, 45, tzinfo=timezone.utc))
        room_events_2["3"] = RoomEvent(uid="3", room=room, name="Event 3",
                                       start=datetime(2020, 5, 25, 16, 0, tzinfo=timezone.utc),
                                       end=datetime(2020, 5, 25, 17, 30, tzinfo=timezone.utc))
        room_events_2["4"] = RoomEvent(uid="4", room=room, name="Event 4",
                                       start=datetime(2020, 5, 25, 14, 15, tzinfo=timezone.utc),
                                       end=datetime(2020, 5, 25, 15, 45, tzinfo=timezone.utc))

        context._strategy._update_room_events_in_db(room_events_2, room.pk)

        room_events_2_from_db = list(RoomEvent.objects.filter(room=room.pk))

        self.assertListEqual(list(room_events_2.values()),
                             room_events_2_from_db,
                             'room_events_2 was not stored and retrieved from DB properly e.g. '
                             'room_events_1["1"] was not removed'
                             'room_events_1["4"] was included')

        for room_event in room_events_2_from_db:
            if room_event.uid == "3":
                self.assertEqual(room_event.uid, room_events_2["3"].uid,
                                 "uid was not properly updated in DB between 1. and 2. room_event data sets")
                self.assertEqual(room_event.name, room_events_2["3"].name,
                                 "name was not properly updated in DB between 1. and 2. room_event data sets")
                self.assertEqual(room_event.start, room_events_2["3"].start,
                                 "start was not properly updated in DB between 1. and 2. room_event data sets")
                self.assertEqual(room_event.end, room_events_2["3"].end,
                                 "end was not properly updated in DB between 1. and 2. room_event data sets")
                self.assertEqual(room_event.room, room_events_2["3"].room,
                                 "room was not properly updated in DB between 1. and 2. room_event data sets")
