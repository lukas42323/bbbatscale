from django.test import TestCase
from core.models import Room, Tenant, Server, get_default_room_config
from core.constants import SCHEDULING_STRATEGY_LEAST_UTILIZATION, SERVER_STATE_UP


class TenantTestCase(TestCase):

    def setUp(self):
        self.fbi = Tenant.objects.create(
            name="Fachbereich Informatik",
            description="Bilden Informatiker aus",
            notifications_emails="FBI Notification E-Mail",
            scheduling_strategy=SCHEDULING_STRATEGY_LEAST_UTILIZATION,
        )

        self.bbb_server = Server.objects.create(
            tenant=self.fbi,
            dns="example.org",
            state=SERVER_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_d19_304 = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server,
            name="D19/304",
            participant_count=10,
            videostream_count=5,
            config=get_default_room_config()
        )

    def test__str__(self):
        c_tenant = Tenant(name="1337")
        self.assertEqual(c_tenant.__str__(), "1337")

    def test_get_current_participant_count(self):
        self.assertEqual(self.fbi.get_current_participant_count(), 10)

    def test_get_utilization(self):
        self.assertEqual(self.fbi.get_utilization(), 7)

    def test_get_utilization_max(self):
        self.assertEqual(self.fbi.get_utilization_max(), 2)

    def test_get_utilization_percent(self):
        self.assertEqual(self.fbi.get_utilization_percent(), 350)

    def test_get_server_for_room(self):
        # alibi
        self.assertEqual(self.fbi.get_server_for_room(), self.bbb_server)

    def test_get_servers_up(self):
        # since there is only server with the state of "server state up"
        # the length of the queryset should be 1
        self.assertEqual(len((self.fbi.get_servers_up())), 1)


class ServerTestCase(TestCase):

    def setUp(self):
        self.fbi = Tenant.objects.create(
            name="Fachbereich Informatik",
        )

        self.bbb_server = Server.objects.create(
            tenant=self.fbi,
            dns="example.org",
            state=SERVER_STATE_UP,
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_d19_304 = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server,
            name="D19/304",
            participant_count=10,
            videostream_count=5,
            config=get_default_room_config()
        )

    def test__str__(self):
        self.assertEqual(self.bbb_server.__str__(), "example.org")

    def test_get_utilization(self):
        self.assertEqual(self.bbb_server.get_utilization(), 7.0)

    def test_get_utilization_percent(self):
        self.assertEqual(self.bbb_server.get_utilization_percent(), 350)

    def test_collect_stats(self):
        pass

    def test_get_participant_count(self):
        self.assertEqual(self.bbb_server.get_participant_count(), 10)

    def test_get_videostream_count(self):
        self.assertEqual(self.bbb_server.get_videostream_count(), 5)


class RoomTestCase(TestCase):

    def setUp(self):
        self.fbi = Tenant.objects.create(
            name="Fachbereich Informatik",
        )

        self.bbb_server = Server.objects.create(
            tenant=self.fbi,
            dns="example.org",
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_d19_304 = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server,
            name="D19/304",
            participant_count=10,
            videostream_count=5,
            config=get_default_room_config()
        )

    def test__str__(self):
        self.assertEqual(self.room_d19_304.__str__(), "D19/304")

    def test_get_participants_current(self):
        self.assertEqual(self.room_d19_304.get_participants_current(), 10)

    def test_get_total_instantiation_counter(self):
        pass

    def test_is_meeting_running(self):
        pass

    def test_get_meeting_infos(self):
        pass
