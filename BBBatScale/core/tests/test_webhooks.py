import hmac
import json
import pytest
from freezegun import freeze_time
import responses

from core.constants import WEBHOOK_SUPPORT_CHAT_INCOMING_MESSAGE
from core.webhooks import webhook_enqueue
from core.models import Webhook


@pytest.fixture(scope='function')
def webhook_incoming_message():
    return Webhook.objects.create(
        name='testing',
        url='https://webhook.test/path',
        event=WEBHOOK_SUPPORT_CHAT_INCOMING_MESSAGE,
        secret='abcdef')


@freeze_time('2020-04-26T16:29:55+00:00')
@responses.activate
@pytest.mark.django_db
def test_webhook_enqueue(webhook_incoming_message):
    expect_mac = bytes.fromhex('''
        12e79ad4137f51306e0b67d1721fb17d40efff81bcda5e734b
        1e954d3b29921af1bbc476402d62bc19f7e807e5799b9337cc
        e82a1f1c212415336d5c4a172eba''')
    expect_timestamp = 1587918595

    def handle(req):
        assert req.headers['Content-Type'] == 'application/json'
        assert 'X-Hook-Signature' in req.headers
        t, v1 = map(
            lambda x: x[1],
            sorted(
                filter(
                    lambda x: x[0] in ['t', 'v1'],
                    map(lambda x: x.split('='),
                        req.headers.get('X-Hook-Signature').split(',')))))

        assert int(t) == expect_timestamp
        assert hmac.compare_digest(bytes.fromhex(v1), expect_mac)

        body = json.loads(req.body)
        assert body['event'] == WEBHOOK_SUPPORT_CHAT_INCOMING_MESSAGE
        assert body['ts'] == expect_timestamp
        assert body['payload'] == {'answer': 42}

        return (200, {}, '')

    responses.add_callback(
        responses.POST,
        'https://webhook.test/path',
        callback=handle,
        content_type='application/json',
    )

    webhook_enqueue(
        WEBHOOK_SUPPORT_CHAT_INCOMING_MESSAGE, {'answer': 42},
        queue_args={'is_async': False})


@freeze_time('2020-04-26T16:29:55+00:00')
@responses.activate
@pytest.mark.django_db
def test_webhook_enqueue_nohook(webhook_incoming_message):
    # Test the property that no request is made when signaling an event with no
    # hooks registered for it. Ensured by responses failing any uses of the
    # requests library when no response/handler is set up.
    webhook_enqueue('OTHER', {'answer': 42}, queue_args={'is_async': False})
