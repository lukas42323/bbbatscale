from __future__ import annotations

import logging
from datetime import timedelta
from math import ceil
from typing import Optional

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db import transaction
from django.db.models import Sum
from django.db.models.signals import m2m_changed
from django.templatetags.static import static
from django.utils.crypto import get_random_string
from django.utils.timezone import now
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _

from core.constants import SERVER_STATES, SERVER_STATE_WAITING_RESPONSE, SERVER_STATE_ERROR, \
    SCHEDULING_STRATEGY_LEAST_PARTICIPANTS, SERVER_STATE_UP, SERVER_STATE_DISABLED, \
    SCHEDULING_STRATEGY_LEAST_UTILIZATION, GUEST_POLICY, GUEST_POLICY_ALLOW, ROOM_STATE, ROOM_STATE_INACTIVE, \
    SCHEDULING_STRATEGIES, ROOM_STATE_ACTIVE, WEBHOOKS
from core.utils import BigBlueButton

logger = logging.getLogger(__name__)


def get_default_room_config():
    return RoomConfiguration.objects.get_or_create(name="Default")[0]


class User(AbstractUser):
    theme = models.ForeignKey("Theme", on_delete=models.SET_NULL, null=True)
    personal_rooms_max_number = models.IntegerField(null=True, blank=True)

    def __str__(self) -> str:
        if self.first_name and self.last_name:
            return f"{self.last_name}, {self.first_name}"
        elif self.first_name:
            return self.first_name
        elif self.last_name:
            return self.last_name
        elif self.email:
            return self.email
        else:
            return gettext("Unknown")

    def get_max_number_of_personal_rooms(self):
        if self.personal_rooms_max_number is not None:
            return self.personal_rooms_max_number
        else:
            general_parameters: GeneralParameter = GeneralParameter.load()
            if self.groups.filter(name=settings.MODERATORS_GROUP).exists():
                return general_parameters.personal_rooms_teacher_max_number
            else:
                return general_parameters.personal_rooms_non_teacher_max_number

    def get_theme(self) -> Theme:
        theme = self.theme
        if theme is None:
            theme = GeneralParameter.load().default_theme
        return theme


class Tenant(models.Model):
    name = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255, null=True, blank=True)
    notifications_emails = models.TextField(null=True, blank=True)
    token_registration = models.CharField(max_length=255, null=True, blank=True)
    scheduling_strategy = models.CharField(max_length=255, choices=SCHEDULING_STRATEGIES,
                                           default=SCHEDULING_STRATEGY_LEAST_UTILIZATION)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.token_registration:
            self.token_registration = get_random_string(length=10)
        super(Tenant, self).save(*args, **kwargs)

    def get_current_participant_count(self):
        return self.room_set.all().aggregate(Sum('participant_count'))['participant_count__sum']

    def get_utilization(self):
        usage = 0
        for server in self.get_servers_up():
            usage = usage + server.get_utilization()
        return ceil(usage)

    def get_utilization_max(self):
        return ceil(sum(map(lambda server: server.videostream_count_max, self.get_servers_up())))

    def get_utilization_percent(self):
        utilization_max = self.get_utilization_max()
        return 0 if utilization_max <= 0 else int(round(self.get_utilization() / utilization_max, 2) * 100)

    def get_server_for_room(self):
        server_to_be = None

        logger.info("Selecting new server for scheduling with strategy: {}".format(self.scheduling_strategy))

        if self.scheduling_strategy == SCHEDULING_STRATEGY_LEAST_PARTICIPANTS:
            min_count = 1000
            for server in self.server_set.filter(state=SERVER_STATE_UP):
                pc = server.get_participant_count()
                if pc < min_count:
                    min_count = pc
                    server_to_be = server
                    logger.info("Best candidate so far: {}".format(server_to_be))

        if self.scheduling_strategy == SCHEDULING_STRATEGY_LEAST_UTILIZATION:
            min_count = 1000
            for server in self.server_set.filter(state=SERVER_STATE_UP):
                pc = server.get_utilization()
                if pc < min_count:
                    min_count = pc
                    server_to_be = server
                    logger.info("Best candidate so far: {}".format(server_to_be))

        logger.info("Selected : {}".format(server_to_be))

        return server_to_be

    def get_servers_up(self):
        return self.server_set.filter(state=SERVER_STATE_UP)


class RoomConfiguration(models.Model):
    name = models.CharField(max_length=255, unique=True)
    mute_on_start = models.BooleanField(default=True)
    all_moderator = models.BooleanField(default=False)
    everyone_can_start = models.BooleanField(default=False)
    authenticated_user_can_start = models.BooleanField(default=False)
    guest_policy = models.CharField(max_length=30, choices=GUEST_POLICY, default=GUEST_POLICY_ALLOW, blank=True)
    allow_guest_entry = models.BooleanField(default=False)
    access_code = models.CharField(max_length=255, null=True, blank=True)
    access_code_guests = models.CharField(max_length=255, null=True, blank=True)
    disable_cam = models.BooleanField(default=False)
    disable_mic = models.BooleanField(default=False)
    allow_recording = models.BooleanField(default=False)
    disable_private_chat = models.BooleanField(default=False)
    disable_public_chat = models.BooleanField(default=False)
    disable_note = models.BooleanField(default=False)
    url = models.CharField(max_length=255, null=True, blank=True)
    dialNumber = models.CharField(max_length=255, null=True, blank=True)
    logoutUrl = models.CharField(max_length=255, null=True, blank=True)
    welcome_message = models.CharField(max_length=255, null=True, blank=True)
    maxParticipants = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class ServerType(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class Server(models.Model):
    tenant = models.ForeignKey(Tenant, on_delete=models.SET_NULL, null=True)
    server_types = models.ManyToManyField(ServerType)
    dns = models.CharField(max_length=255, unique=True)
    state = models.CharField(max_length=255, choices=SERVER_STATES, default=SERVER_STATE_WAITING_RESPONSE)
    datacenter = models.CharField(max_length=255, null=True, blank=True)
    shared_secret = models.CharField(max_length=255, null=True, blank=True)
    participant_count_max = models.PositiveIntegerField(default=250)
    videostream_count_max = models.PositiveIntegerField(default=25)

    last_health_check = models.DateTimeField(null=True, blank=True)

    class Meta:
        ordering = ('tenant', 'dns',)

    def __str__(self):
        return self.dns

    # quick and dirty util cals.
    def get_utilization(self):
        audio_video_factor = self.participant_count_max / self.videostream_count_max
        audio_usage = self.get_participant_count() / audio_video_factor
        video_usage = self.get_videostream_count()
        total_usage = audio_usage + video_usage

        return 0 if total_usage <= 0 else total_usage

    def get_utilization_percent(self):
        return int(round((self.get_utilization()) / self.videostream_count_max, 2) * 100)

    # todo change to service.py
    def collect_stats(self):
        logger.info("Health Check started for.... {}".format(self.dns))
        # skip disabled servers
        if self.state != SERVER_STATE_DISABLED:
            try:
                bbb = BigBlueButton(self.dns, self.shared_secret)
                meetings = bbb.validate_get_meetings(bbb.get_meetings())

                if meetings:
                    for meeting in meetings:
                        logger.info("Starting transaction for updating room {}".format(meeting['meetingName']))
                        with transaction.atomic():
                            default_arguments = {
                                'server': self,
                                'meeting_id': meeting['meetingID'],
                                'state': ROOM_STATE_ACTIVE,
                                'attendee_pw': meeting['attendeePW'],
                                'moderator_pw': meeting['moderatorPW'],
                                'tenant': self.tenant,
                                'participant_count': meeting['participantCount'],
                                'videostream_count': meeting['videoCount'],
                                'is_breakout': True if meeting['isBreakout'] == 'true' else False,
                                'last_running': now(),
                            }
                            if "bbb-origin" in meeting["metadata"].keys() and \
                                    meeting["metadata"]["bbb-origin"] == "Moodle":
                                MoodleRoom.objects.update_or_create(name=meeting['meetingName'],
                                                                    defaults=default_arguments)
                            else:
                                Room.objects.update_or_create(name=meeting['meetingName'],
                                                              defaults=default_arguments)
                        logger.info("Ending transaction for updating room {}".format(meeting['meetingName']))

                self.state = SERVER_STATE_UP

            except Exception as e:
                logger.error(e)

                logger.error("setting server state of {} to ERROR".format(self.dns))
                self.state = SERVER_STATE_ERROR

            self.last_health_check = now()
            self.save()
        logger.info("Health Check finished for.... {}".format(self.dns))

    def get_participant_count(self):
        count = self.room_set.all().aggregate(Sum('participant_count'))['participant_count__sum']
        return count if count else 0

    def get_videostream_count(self):
        count = self.room_set.all().aggregate(Sum('videostream_count'))['videostream_count__sum']
        return count if count else 0


class Room(models.Model):
    # Check on delete
    tenant = models.ForeignKey(Tenant, on_delete=models.PROTECT)
    server = models.ForeignKey(Server, on_delete=models.SET_NULL, null=True, blank=True)
    name = models.CharField(max_length=255, unique=True)
    # Check on delete
    config = models.ForeignKey(RoomConfiguration, on_delete=models.SET(get_default_room_config))
    meeting_id = models.CharField(max_length=255, unique=True)
    is_public = models.BooleanField(default=True)
    state = models.CharField(max_length=30, choices=ROOM_STATE, default=ROOM_STATE_INACTIVE)

    # Room Config
    mute_on_start = models.BooleanField(default=True)
    all_moderator = models.BooleanField(default=False)
    everyone_can_start = models.BooleanField(default=False)
    authenticated_user_can_start = models.BooleanField(default=False)
    guest_policy = models.CharField(max_length=30, choices=GUEST_POLICY, default=GUEST_POLICY_ALLOW, blank=True)
    allow_guest_entry = models.BooleanField(default=False)
    access_code = models.CharField(max_length=255, null=True, blank=True)
    access_code_guests = models.CharField(max_length=255, null=True, blank=True)
    disable_cam = models.BooleanField(default=False)
    disable_mic = models.BooleanField(default=False)
    allow_recording = models.BooleanField(default=False)
    disable_private_chat = models.BooleanField(default=False)
    disable_public_chat = models.BooleanField(default=False)
    disable_note = models.BooleanField(default=False)
    url = models.CharField(max_length=255, null=True, blank=True)
    dialNumber = models.CharField(max_length=255, null=True, blank=True)
    logoutUrl = models.CharField(max_length=255, null=True, blank=True)
    welcome_message = models.CharField(max_length=255, null=True, blank=True)

    attendee_pw = models.CharField(max_length=255, null=True, blank=True)
    moderator_pw = models.CharField(max_length=255, null=True, blank=True)

    comment_public = models.CharField(max_length=255, null=True, blank=True)
    comment_private = models.CharField(max_length=255, null=True, blank=True)

    click_counter = models.PositiveIntegerField(default=0)

    participant_count = models.PositiveIntegerField(default=0)
    videostream_count = models.PositiveIntegerField(default=0)

    is_breakout = models.BooleanField(default=False)

    event_collection_strategy = models.CharField(max_length=255, null=True, blank=True)
    event_collection_parameters = models.TextField(null=True, blank=True)

    last_meeting_creating = models.DateTimeField(null=True, blank=True)
    last_running = models.DateTimeField(null=True, blank=True)

    maxParticipants = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name

    @staticmethod
    def get_participants_current():
        return Room.objects.all().aggregate(Sum('participant_count'))['participant_count__sum']

    @staticmethod
    def get_total_instantiation_counter():
        return Room.objects.all().aggregate(Sum('instantiation_counter'))['instantiation_counter__sum']

    def is_meeting_running(self):
        bbb = BigBlueButton(self.server.dns, self.server.shared_secret)
        return bbb.is_meeting_running(self.meeting_id)

    def get_meeting_infos(self):
        bbb = BigBlueButton(self.server.dns, self.server.shared_secret)
        bbb.get_meeting_infos(self.meeting_id)

    def end_meeting(self):
        bbb = BigBlueButton(self.server.dns, self.server.shared_secret)
        return bbb.end(self.meeting_id, self.moderator_pw)

    def is_room_only(self):
        is_subclass = self.is_homeroom() or self.is_personalroom() or self.is_moodleroom()
        return not is_subclass

    def is_homeroom(self):
        try:
            return self.homeroom is not None
        except Room.homeroom.RelatedObjectDoesNotExist:
            return False

    def is_personalroom(self):
        try:
            return self.personalroom is not None
        except Room.personalroom.RelatedObjectDoesNotExist:
            return False

    def is_moodleroom(self):
        try:
            return self.moodleroom is not None
        except Room.moodleroom.RelatedObjectDoesNotExist:
            return False


class MoodleRoom(Room):
    def __str__(self):
        return self.name


class HomeRoom(Room):
    owner = models.OneToOneField(User, on_delete=models.CASCADE, null=False)


class PersonalRoom(Room):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    co_owners = models.ManyToManyField(User, related_name="co_owners", blank=True)

    def has_co_owner(self, user: User):
        return user in self.co_owners.all()

    @staticmethod
    def co_owner_shall_not_contain_owner(**kwargs):
        action: str = kwargs["action"]

        if action == "pre_add":
            instance: PersonalRoom = kwargs["instance"]
            pk_set: set = kwargs["pk_set"]

            if instance.owner.id in pk_set:
                logger.warning("Prevented storing owner ({}) as co-owner for room {}.".format(instance.owner, instance))
                pk_set.remove(instance.owner.id)


m2m_changed.connect(PersonalRoom.co_owner_shall_not_contain_owner, sender=PersonalRoom.co_owners.through)


class RoomEvent(models.Model):
    uid = models.CharField(max_length=255, primary_key=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, null=True)
    start = models.DateTimeField()
    end = models.DateTimeField()

    def __str__(self):
        return "{} – {}".format(self.room, self.name)

    class Meta:
        ordering = ('room',)


class GeneralParameter(models.Model):
    latest_news = models.TextField(null=True, blank=True, verbose_name="Latest news", help_text=_(
        "This text will be shown in a red alert box on the start page. "
        "You can use it to inform your users about critical news."))
    participant_total_max = models.PositiveIntegerField(default=0)
    jitsi_enable = models.BooleanField(default=False, verbose_name=_("Enable Jitsi ad-hoc meeting"), help_text=_(
        "If checked a button for ad-hoc meeting creation using Jitsi is enabled."
        " If enabled, please make sure to also set a target jisti server in the jits_url field"))
    jitsi_url = models.CharField(max_length=255, blank=True, null=True, default="https://meet.jit.si/",
                                 verbose_name=_("JITSI URL"),
                                 help_text=_(
                                     "Defines the target of the create ad-hoc meeting button on the start page."))
    playback_url = models.CharField(max_length=255, blank=True, null=True)
    faq_url = models.CharField(max_length=255, blank=True, null=True, verbose_name=_("FAQ URL"), help_text=_(
        "Defines the target of the read FAQ button on the start page."))
    # TODO Check if still used. Dont think so....
    feedback_email = models.CharField(max_length=255, blank=True, null=True)
    footer = models.TextField(blank=True, null=True, verbose_name=_("Footer"), help_text=_(
        "Defines the footer text of the application"))
    logo_link = models.CharField(max_length=510, blank=True, null=True, verbose_name=_("Logo URL"), help_text=_(
        "The image behind this link will be used as logo on this application."))
    favicon_link = models.CharField(max_length=510, blank=True, null=True, verbose_name=_("FAV Icon URL"), help_text=_(
        "The image behind this link will be used as fav icon on this application."))
    page_title = models.CharField(max_length=510, blank=True, null=True, default="bbb@scale | Virtual Rooms",
                                  verbose_name=_("Page Title"),
                                  help_text=_("Define the title of this application."))
    home_room_enabled = models.BooleanField(default=False, verbose_name=_("Enable Home room"), help_text=_(
        "If checked the Home room feature is enabled and users "
        "have access to their personal room via a button on the start page."))
    home_room_teachers_only = models.BooleanField(default=True, verbose_name=_("Teachers only"), help_text=_(
        "If checked only teachers are allowed to access their private rooms."))
    home_room_tenant = models.ForeignKey(Tenant, null=True, blank=True, on_delete=models.SET_NULL,
                                         verbose_name=_("Tenant"),
                                         help_text=_("Define which tenant will be used for Home rooms."))
    home_room_room_configuration = models.ForeignKey(RoomConfiguration, null=True, blank=True,
                                                     on_delete=models.SET_NULL, verbose_name=_("Room configuration"),
                                                     help_text=_(
                                                         "Define which room configuration will be " +
                                                         "used for Home rooms."))
    default_theme = models.ForeignKey("Theme", on_delete=models.PROTECT, verbose_name=_("Default Theme"),
                                      help_text=_(
                                          "Define which theme should be the default theme for users " +
                                          "who have not changed their theme yet or for non logged in users."))
    personal_rooms_enabled = models.BooleanField(default=False, verbose_name=_("Enable personal room"), help_text=_(
        "If checked the personal room feature is enabled and users "
        "can create personal rooms"))
    personal_rooms_tenant = models.ForeignKey(Tenant, null=True, blank=True, on_delete=models.SET_NULL,
                                              related_name='personal_room_tenant',
                                              verbose_name=_("Personal Room Tenant"),
                                              help_text=_("Define which tenant will be used for personal rooms."))
    personal_rooms_teacher_max_number = models.IntegerField(default=0,
                                                            verbose_name=_("Teacher max number"),
                                                            help_text=_(
                                                                "Defines the maximum number of rooms a teacher can "
                                                                "create. Can be overwritten on a per user basis using "
                                                                "the user field."))
    personal_rooms_non_teacher_max_number = models.IntegerField(default=0,
                                                                verbose_name=_("Non-teacher max number"),
                                                                help_text=_(
                                                                    "Defines the maximum number of rooms a non-teacher "
                                                                    "can create. "
                                                                    "Can be overwritten on a per user basis "
                                                                    "using the user field."))

    def save(self, *args, **kwargs):
        self.pk = 1
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        return cls.objects.get_or_create(pk=1, defaults={"default_theme": Theme.objects.order_by("pk").first()})[0]


class Meeting(models.Model):
    room_name = models.CharField(max_length=255, null=True, blank=True)
    meeting_id = models.CharField(max_length=255, null=True, blank=True)
    creator = models.CharField(max_length=255, null=True, blank=True)
    started = models.DateTimeField(auto_now_add=True)
    replay_id = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return "{}-{}-{}".format(self.started.date(), self.room_name, self.creator)

    class Meta:
        ordering = ('-started',)


class Theme(models.Model):
    name = models.CharField(max_length=255, unique=True)
    base_stylesheet_static_path = models.TextField(null=True)
    multi_select_stylesheet_static_path = models.TextField(null=True)

    def __str__(self) -> str:
        return self.name

    def __getattr__(self, element: str) -> Optional[str]:
        try:
            static_path = self.__getattribute__(element + "_static_path")
        except AttributeError as exc:
            exc.args = (exc.args[0].replace("_static_path", ""), *exc.args[1:])
            raise

        if static_path is None:
            return None

        return static(static_path)


class Webhook(models.Model):
    name = models.CharField(
        max_length=255, unique=True, help_text=_('Free-text name field'))

    url = models.CharField(
        max_length=255, help_text=_("The webhook receiver's URL"))

    event = models.CharField(
        max_length=255,
        choices=WEBHOOKS,
        help_text=_('The event this Webhook is triggered on'),
    )

    enabled = models.BooleanField(
        default=True,
        help_text=_('Whether this Webhook is active'),
    )

    secret = models.CharField(
        max_length=255,
        help_text=_(
            'Hex-encoded secret for authenticating webhooks. '
            'Used to compute a HMAC-SHA512 over the request body and a timestamp. '
            'The HMAC (but not the secret!) is included with the request in '
            'the X-Hook-Signature header.'))

    timeout = models.DurationField(
        default=timedelta(seconds=10),
        help_text=_('Set a timeout for the webhook request to complete'),
    )

    num_retries = models.IntegerField(
        default=3,
        help_text=_('Retry failed execution this many times'),
    )

    def __str__(self) -> str:
        return self.name
