import csv
import json
import logging

from django.conf import settings
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db.models import Field, Q
from django.db.transaction import atomic, non_atomic_requests
from django.db.utils import IntegrityError
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse, HttpResponseNotAllowed, HttpRequest
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_exempt

from core.constants import ROOM_STATE_ACTIVE, ROOM_STATE_INACTIVE, ROOM_STATE_CREATING
from core.filters import RoomFilter, ServerFilter, TenantFilter, UserFilter
from core.models import Server, Room, GeneralParameter, Tenant, RoomConfiguration, Meeting, MoodleRoom, User, Theme, \
    ServerType, PersonalRoom, HomeRoom
from core.services import meeting_create, create_join_meeting_url, reset_room, translate_bbb_meta_data, room_click, \
    get_rooms_with_current_next_event, moodle_get_or_create_room_with_params, get_join_password, \
    parse_recordings_from_multiple_sources, parse_dict_to_xml, join_or_create_meeting_permissions
from core.utils import validate_csv, validate_bbb_api_call, BigBlueButton, \
    bbb_checksum_error_xml_response, bbb_meeting_not_found, bbb_no_recordings_meeting, set_room_config
from .forms import ServerForm, RoomForm, PersonalRoomForm, RoomConfigurationForm, TenantForm, ConfigureRoomForm, \
    RoomFilterFormHelper, \
    ServerFilterFormHelper, TenantFilterFormHelper, UserForm, UserUpdateForm, PasswordChangeCrispyForm, \
    GeneralParametersForm, PersonalRoomCoOwnerForm, HomeRoomForm

logger = logging.getLogger(__name__)


def home(request):
    logger.info("home.html accessed by user {}".format(request.user.username))
    rooms = get_rooms_with_current_next_event()
    return render(request, 'home.html', {'servers': Server.objects.all(),
                                         'rooms': rooms,
                                         'participants_current': Room.get_participants_current(),
                                         'performed_meetings': Meeting.objects.all().count(),
                                         'base_url': settings.BASE_URL,
                                         })


def statistics(request):
    return render(request, 'statistics.html', {'servers': Server.objects.all(),
                                               'rooms': Room.objects.all(),
                                               'participants_current': Room.get_participants_current(),
                                               'performed_meetings': Meeting.objects.all().count(),
                                               'tenants': Tenant.objects.all()})


@login_required
@staff_member_required
def settings_edit(request):
    form = GeneralParametersForm(request.POST)
    if request.method == "POST" and form.is_valid():
        form.save()
        messages.success(request, _("Settings successfully saved."))
        return redirect('settings_edit')
    else:
        form = GeneralParametersForm(instance=GeneralParameter.load())
    return render(request, 'settings.html', {'form': form})


@login_required
@staff_member_required
def import_export(request):
    return render(request, 'import_export.html')


@login_required
@staff_member_required
def import_upload_json(request):  # noqa: C901 TODO
    if request.method == 'POST':
        try:
            upload_file = request.FILES['document']
            file = json.load(upload_file)
            room_configuration_list = []
            room_configuration_updates = []
            tenant_list = []
            tenant_updates = []
            server_list = []
            server_updates = []
            room_list = []
            room_updates = []
            homeroom_list = []
            homeroom_updates = []
            meeting_list = []
            meeting_updates = []
            with atomic():
                for user in file['users']:
                    _user, created = User.objects.update_or_create(
                        username=user['username'],
                        defaults={
                            "email": user['email'],
                            "password": user['password'],
                            "first_name": user['first_name'],
                            "last_name": user['last_name'],
                            "is_active": user['is_active'],
                            "is_staff": user['is_staff'],
                            "is_superuser": user['is_superuser'],
                            "last_login": user['last_login'],
                            "date_joined": user['date_joined']
                        }
                    )
                for room_configuration in file["room_configurations"]:
                    # create or update all room_configurations belonging to current tenant
                    _room_configuration, created = RoomConfiguration.objects.update_or_create(
                        name=room_configuration["name"],
                        defaults={
                            "name": room_configuration["name"],
                            "mute_on_start": room_configuration["mute_on_start"],
                            "all_moderator": room_configuration["all_moderator"],
                            "everyone_can_start": room_configuration["everyone_can_start"],
                            "authenticated_user_can_start": room_configuration["authenticated_user_can_start"],
                            "guest_policy": room_configuration["guest_policy"],
                            "allow_guest_entry": room_configuration["allow_guest_entry"],
                            "access_code": room_configuration["access_code"],
                            "access_code_guests": room_configuration["access_code_guests"],
                            "disable_cam": room_configuration["disable_cam"],
                            "disable_mic": room_configuration["disable_mic"],
                            "allow_recording": room_configuration["allow_recording"],
                            "disable_private_chat": room_configuration["disable_private_chat"],
                            "disable_public_chat": room_configuration["disable_public_chat"],
                            "disable_note": room_configuration["disable_note"],
                            "url": room_configuration["url"],
                            "maxParticipants": room_configuration["maxParticipants"] if "maxParticipants" in
                                                                                        room_configuration else None
                        }
                    )
                    room_configuration_list.append(
                        _room_configuration.name) if created else room_configuration_updates.append(
                        _room_configuration.name)

                for i in range(len(file["tenant"])):
                    # create or update all tenants in json
                    _tenant, created = Tenant.objects.update_or_create(
                        name=file["tenant"][i]["name"],
                        defaults={
                            'description': file["tenant"][i]['description'],
                            'notifications_emails': file["tenant"][i]['notifications_emails'],
                            'token_registration': file["tenant"][i]['token_registration'],
                            'scheduling_strategy': file["tenant"][i]['scheduling_strategy'],
                        }
                    )
                    tenant_list.append(_tenant.name) if created else tenant_updates.append(_tenant.name)

                    for server in file["tenant"][i]["servers"]:
                        # create or update all servers belonging to current tenant
                        _server, created = Server.objects.update_or_create(
                            dns=server['dns'],
                            defaults={
                                'tenant': _tenant,
                                'datacenter': server['datacenter'],
                                'shared_secret': server['shared_secret'],
                                'participant_count_max': server['participant_count_max'],
                                'videostream_count_max': server['videostream_count_max'],
                            }
                        )
                        server_list.append(_server.dns) if created else server_updates.append(_server.dns)

                    for room in file["tenant"][i]["rooms"]:
                        # create or update all rooms belonging to current tenant
                        if room["config_name"] is not None:
                            room_configuration = RoomConfiguration.objects.get(name=room["config_name"])
                        else:
                            room_configuration = None
                        _room, created = Room.objects.update_or_create(
                            name=room['name'],
                            defaults={
                                'tenant': _tenant,
                                'is_public': room['is_public'],
                                'comment_public': room['comment_public'],
                                'comment_private': room['comment_private'],
                                'config': room_configuration,
                                'click_counter': room["click_counter"],
                                'event_collection_strategy': room["event_collection_strategy"],
                                'event_collection_parameters': room["event_collection_parameters"],
                                "maxParticipants": room["maxParticipants"] if "maxParticipants" in room else None
                            }
                        )
                        room_list.append(_room.name) if created else room_updates.append(_room.name)

                for i in range(len(file["servers"])):
                    _server, created = Server.objects.update_or_create(
                        dns=file["servers"][i]['dns'],
                        defaults={
                            'tenant': None,
                            'datacenter': file["servers"][i]['datacenter'],
                            'shared_secret': file["servers"][i]['shared_secret'],
                            'participant_count_max': file["servers"][i]['participant_count_max'],
                            'videostream_count_max': file["servers"][i]['videostream_count_max'],
                        }
                    )
                    server_list.append(_server.dns) if created else server_updates.append(_server.dns)

                for i in range(len(file["rooms"])):
                    # create or update all rooms belonging to current tenant
                    if file["rooms"][i]["config_name"] is not None:
                        room_configuration = RoomConfiguration.objects.get(name=file["rooms"][i]["config_name"])
                    else:
                        room_configuration = None
                    _room, created = Room.objects.update_or_create(
                        name=file["rooms"][i]['name'],
                        defaults={
                            'tenant': None,
                            'is_public': file["rooms"][i]['is_public'],
                            'comment_public': file["rooms"][i]['comment_public'],
                            'comment_private': file["rooms"][i]['comment_private'],
                            'config': room_configuration,
                            'click_counter': file["rooms"][i]["click_counter"],
                            'event_collection_strategy': file["rooms"][i]["event_collection_strategy"],
                            'event_collection_parameters': file["rooms"][i]["event_collection_parameters"],
                        }
                    )
                    room_list.append(_room.name) if created else room_updates.append(_room.name)

                for homeroom in file["homerooms"]:
                    # create or update all homerooms belonging to current tenant
                    try:
                        HomeRoom.objects.get(name=homeroom["name"])
                        created = True
                    except HomeRoom.DoesNotExist:
                        room = Room.objects.get(name=homeroom["name"])
                        owner_id = User.objects.get(username=homeroom["owner_username"]).id
                        homeroom_kwargs = dict(
                            ((room_field.attname, getattr(room, room_field.attname)) for room_field in
                             room._meta.get_fields(include_parents=False, include_hidden=False)
                             if isinstance(room_field, Field) and not room_field.primary_key),
                            room_ptr_id=room.id, owner_id=owner_id)
                        HomeRoom(**homeroom_kwargs).save_base(raw=True, force_insert=True)
                        created = False

                    homeroom_list.append(homeroom["name"]) if created else homeroom_updates.append(homeroom["name"])

                for meeting in file["meetings"]:
                    # create or update all meetings belonging to current tenant
                    _meeting = Meeting.objects.create(
                        room_name=meeting["room_name"],
                        meeting_id=meeting["meeting_id"],
                        creator=meeting["creator"],
                        replay_id=meeting["replay_id"],
                    )
                    _meeting.started = meeting["started"]
                    _meeting.save()
                    meeting_list.append(_meeting.room_name)

                general_parameter = file["general_parameter"]
                _general_parameter = GeneralParameter.load()

                _general_parameter.faq_url = general_parameter["faq_url"]
                _general_parameter.favicon_link = general_parameter["favicon_link"]
                _general_parameter.feedback_email = general_parameter["feedback_email"]
                _general_parameter.footer = general_parameter["footer"]
                _general_parameter.home_room_enabled = general_parameter["home_room_enabled"]
                _general_parameter.home_room_room_configuration = (
                    RoomConfiguration.objects.get(name=general_parameter["home_room_room_configuration_name"])
                    if general_parameter["home_room_room_configuration_name"] is not None else None)
                _general_parameter.home_room_teachers_only = general_parameter["home_room_teachers_only"]
                _general_parameter.home_room_tenant = (
                    Tenant.objects.get(name=general_parameter["home_room_tenant_name"])
                    if general_parameter["home_room_tenant_name"] is not None else None)
                _general_parameter.jitsi_url = general_parameter["jitsi_url"]
                _general_parameter.latest_news = general_parameter["latest_news"]
                _general_parameter.logo_link = general_parameter["logo_link"]
                _general_parameter.page_title = general_parameter["page_title"]
                _general_parameter.participant_total_max = general_parameter["participant_total_max"]
                _general_parameter.playback_url = general_parameter["playback_url"]
                _general_parameter.save()

                return render(request, 'import_export_importsuccess.html',
                              {
                                  'room_configuration_list': room_configuration_list,
                                  'room_configuration_updates': room_configuration_updates,
                                  'server_list': server_list,
                                  'server_updates': server_updates,
                                  'room_list': room_list,
                                  'room_updates': room_updates,
                                  'tenant_list': tenant_list,
                                  'tenant_updates': tenant_updates,
                                  'homeroom_list': homeroom_list,
                                  'homeroom_updates': homeroom_updates,
                                  'meeting_list': meeting_list,
                                  'meeting_updates': meeting_updates,
                              })

        except KeyError as e:
            messages.error(request, e)
            messages.error(request, _('KeyError: make sure all necessary attributes were set'))
            return redirect('import_export')

        except json.JSONDecodeError:
            messages.error(request, _('JSONDecodeError: something must be wrong with your file structure'))
            return redirect('import_export')


@login_required
@staff_member_required
def import_upload_csv(request):
    if request.method == 'POST':
        try:

            csv_file = request.FILES['document']
            room_list = []
            room_updates = []

            if not csv_file.name.endswith('.csv'):
                messages.error(request, 'This is not a csv file')

            decode_file = csv_file.read().decode('utf-8').splitlines()

            uniq_constraints = validate_csv(csv.DictReader(decode_file))

            if any(uniq_constraints):
                return render(request, 'import_export.html', {'uniq_constraints': uniq_constraints})
            else:
                with atomic():
                    for row in csv.DictReader(decode_file):
                        if Tenant.objects.filter(name=row['tenant']).count() >= 1:
                            tenant = Tenant.objects.get(name=row['tenant'])
                            _room, created = Room.objects.update_or_create(
                                name=row['name'],
                                defaults={
                                    'tenant': tenant,
                                    'is_public': row['is_public'],
                                    'comment_public': row['comment_public'],
                                    'comment_private': row['comment_private'],
                                }
                            )
                            room_list.append(_room.name) if created else room_updates.append(_room.name)
                        else:
                            _room, created = Room.objects.update_or_create(
                                name=row['name'],
                                defaults={
                                    'tenant': None,
                                    'is_public': row['is_public'],
                                    'comment_public': row['comment_public'],
                                    'comment_private': row['comment_private'],
                                }
                            )
                            room_list.append(_room.name) if created else room_updates.append(_room.name)

                    return render(request, 'import_export_importsuccess.html',
                                  {'room_list': room_list, 'room_updates': room_updates})
        except KeyError:
            messages.error(request, _('KeyError: make sure all necessary attributes were set'))
            return redirect('import_export')


@login_required
@staff_member_required
def export_download_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="export.csv"'

    writer = csv.writer(response)

    writer.writerow(['tenant', 'name', 'is_public', 'comment_public', 'comment_private'])

    rooms = Room.objects.all().values_list('tenant__name', 'name', 'is_public', 'comment_public', 'comment_private')

    for room in rooms:
        writer.writerow(room)

    return response


@login_required
@staff_member_required
def export_download_json(request):
    room_configurations = RoomConfiguration.objects.filter()
    tenants = Tenant.objects.all()
    room_no_ten = Room.objects.filter(tenant__isnull=True)
    server_no_ten = Server.objects.filter(tenant__isnull=True)
    homerooms = HomeRoom.objects.all()
    meetings = Meeting.objects.all()
    exportfile = {
        "users": [],
        "room_configurations": [],
        "tenant": [],
        "servers": [],
        "rooms": [],
        "homerooms": [],
        "meetings": [],
        "general_parameter": {},
    }
    for user in User.objects.all():
        _user = {
            "username": user.username,
            "email": user.email,
            "password": user.password,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "is_active": user.is_active,
            "is_staff": user.is_staff,
            "is_superuser": user.is_superuser,
            "last_login": user.last_login.isoformat(),
            "date_joined": user.date_joined.isoformat(),
        }
        exportfile["users"].append(_user)
    for _room_configuration in room_configurations:
        # all rooms that belong to no tenant
        room = {
            "name": _room_configuration.name,
            "mute_on_start": _room_configuration.mute_on_start,
            "all_moderator": _room_configuration.all_moderator,
            "everyone_can_start": _room_configuration.everyone_can_start,
            "authenticated_user_can_start": _room_configuration.authenticated_user_can_start,
            "guest_policy": _room_configuration.guest_policy,
            "allow_guest_entry": _room_configuration.allow_guest_entry,
            "access_code": _room_configuration.access_code,
            "access_code_guests": _room_configuration.access_code_guests,
            "disable_cam": _room_configuration.disable_cam,
            "disable_mic": _room_configuration.disable_mic,
            "allow_recording": _room_configuration.allow_recording,
            "disable_private_chat": _room_configuration.disable_private_chat,
            "disable_public_chat": _room_configuration.disable_public_chat,
            "disable_note": _room_configuration.disable_note,
            "url": _room_configuration.url,
            "maxParticipants": _room_configuration.maxParticipants,
        }
        exportfile["room_configurations"].append(room)

    for _tenant in tenants:
        # tenant -> servers/rooms
        tenant = {
            "name": _tenant.name,
            "description": _tenant.description,
            "notifications_emails": _tenant.notifications_emails,
            "token_registration": _tenant.token_registration,
            "scheduling_strategy": _tenant.scheduling_strategy,
            "servers": [],
            "rooms": [],
        }

        for _server in _tenant.server_set.all():
            # all servers that belong to current tenant
            server = {
                "dns": _server.dns,
                "datacenter": _server.datacenter,
                "shared_secret": _server.shared_secret,
                "participant_count_max": _server.participant_count_max,
                "videostream_count_max": _server.videostream_count_max,
            }
            tenant["servers"].append(server)
        for _room in _tenant.room_set.all():
            # all rooms that belong to current tenant
            room = {
                "name": _room.name,
                "meeting_id": _room.meeting_id,
                "is_public": _room.is_public,
                "comment_public": _room.comment_public,
                "comment_private": _room.comment_private,
                "config_name": _room.config.name if _room.config is not None else None,
                "click_counter": _room.click_counter,
                "event_collection_strategy": _room.event_collection_strategy,
                "event_collection_parameters": _room.event_collection_parameters,
            }
            tenant["rooms"].append(room)
        exportfile['tenant'].append(tenant)

    for _server in server_no_ten:
        # all servers that belong to no tenant
        server = {
            "dns": _server.dns,
            "datacenter": _server.datacenter,
            "shared_secret": _server.shared_secret,
            "participant_count_max": _server.participant_count_max,
            "videostream_count_max": _server.videostream_count_max,
        }
        exportfile['servers'].append(server)
    for _room in room_no_ten:
        # all rooms that belong to no tenant
        room = {
            "name": _room.name,
            "meeting_id": _room.meeting_id,
            "is_public": _room.is_public,
            "comment_public": _room.comment_public,
            "comment_private": _room.comment_private,
            "config_name": _room.config.name if _room.config is not None else None,
            "click_counter": _room.click_counter,
            "event_collection_strategy": _room.event_collection_strategy,
            "event_collection_parameters": _room.event_collection_parameters,
        }
        exportfile['rooms'].append(room)

    for _homeroom in homerooms:
        # all rooms that belong to no tenant
        homeroom = {
            "name": _homeroom.name,
            "owner_username": _homeroom.owner.username,
        }
        exportfile["homerooms"].append(homeroom)

    for _meeting in meetings:
        # all rooms that belong to no tenant
        meeting = {
            "room_name": _meeting.room_name,
            "meeting_id": _meeting.meeting_id,
            "creator": _meeting.creator,
            "started": _meeting.started.isoformat(),
            "replay_id": _meeting.replay_id,
        }
        exportfile["meetings"].append(meeting)

    _general_parameter = GeneralParameter.load()
    exportfile["general_parameter"] = {
        "faq_url": _general_parameter.faq_url,
        "favicon_link": _general_parameter.favicon_link,
        "feedback_email": _general_parameter.feedback_email,
        "footer": _general_parameter.footer,
        "home_room_enabled": _general_parameter.home_room_enabled,
        "home_room_room_configuration_name": (
            _general_parameter.home_room_room_configuration.name
            if _general_parameter.home_room_room_configuration is not None else None),
        "home_room_teachers_only": _general_parameter.home_room_teachers_only,
        "home_room_tenant_name": (
            _general_parameter.home_room_tenant.name
            if _general_parameter.home_room_tenant is not None else None),
        "jitsi_url": _general_parameter.jitsi_url,
        "latest_news": _general_parameter.latest_news,
        "logo_link": _general_parameter.logo_link,
        "page_title": _general_parameter.page_title,
        "participant_total_max": _general_parameter.participant_total_max,
        "playback_url": _general_parameter.playback_url
    }

    result = json.dumps(exportfile, indent=4)

    response = HttpResponse(result, content_type='application/json')
    response['Content-Disposition'] = 'attachment; filename="export.json"'

    return response


def join_redirect(request, room):
    try:
        _room = Room.objects.get(name=room)
        room_click(_room.pk)
        return redirect('join_or_create_meeting', _room.meeting_id)
    except Room.DoesNotExist:
        messages.error(request, _("Unable to find Room with given name: '{}'.").format(room))
        return redirect('home')


def join_redirect_deprecated(request, building, room):
    messages.warning(request, _("This endpoint is deprecated and will be removed in the next major release."))
    return join_redirect(request, f'{building}/{room}')


@login_required
@staff_member_required
def servers_overview(request):
    f = ServerFilter(request.GET, queryset=Server.objects.all().prefetch_related('tenant'))
    f.form.helper = ServerFilterFormHelper
    return render(request, 'servers_overview.html', {'filter': f})


@login_required
@staff_member_required
def server_create(request):
    form = ServerForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        instance = form.save(commit=False)

        # Custom form validations
        instance.save()
        instance.server_types.clear()
        for u in form.cleaned_data.get('server_types'):
            instance.server_types.add(u)
        messages.success(request, _("Server: {} was created successfully.").format(instance.dns))
        # return to URL
        return redirect('servers_overview')
    return render(request, 'servers_create.html', {'form': form})


@login_required
@staff_member_required
def server_delete(request, server):
    instance = get_object_or_404(Server, pk=server)
    instance.delete()
    messages.success(request, _("Server was deleted successfully"))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required
@staff_member_required
def server_update(request, server):
    instance = get_object_or_404(Server, pk=server)
    form = ServerForm(request.POST or None, instance=instance)
    if request.method == "POST":
        if form.is_valid():
            _server = form.save(commit=False)

            _server.save()
            _server.server_types.clear()
            for u in form.cleaned_data.get('server_types'):
                _server.server_types.add(u)
            messages.success(request, _("Server: {} was updated successfully.").format(_server.dns))
            return redirect('servers_overview')
    return render(request, 'servers_create.html', {'form': form})


@login_required
@staff_member_required
def rooms_overview(request):
    f = RoomFilter(request.GET, queryset=Room.objects.all().prefetch_related('server', 'config', 'tenant'))
    f.form.helper = RoomFilterFormHelper
    return render(request, 'rooms_overview.html', {'filter': f})


@login_required
@staff_member_required
def room_create(request):
    form = RoomForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        _room = form.save(commit=False)
        # Custom form validations
        if _room.config:
            _room = set_room_config(_room)
        _room.save()

        messages.success(request, _("Room {} was created successfully.").format(_room.name))
        # return to URL
        return redirect('rooms_overview')
    return render(request, 'rooms_create.html', {'form': form})


@login_required
@staff_member_required
def room_details(request, room):
    return render(request, '', {'room': Room.objects.get(pk=room)})


@login_required
@staff_member_required
def room_delete(request, room):
    instance = get_object_or_404(Room, pk=room)
    instance.delete()
    messages.success(request, _("Room was deleted successfully."))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required
@staff_member_required
def room_update(request, room):
    instance = get_object_or_404(Room, pk=room)
    form = RoomForm(request.POST or None, instance=instance)
    if request.method == "POST" and form.is_valid():
        _room = form.save(commit=False)
        if _room.config:
            _room = set_room_config(_room)
        _room.save()
        messages.success(request, _("Room {} was updated successfully.").format(_room.name))
        return redirect('rooms_overview')
    return render(request, 'rooms_create.html', {'form': form})


@login_required
def home_room_update(request, home_room):
    instance = get_object_or_404(HomeRoom, pk=home_room)
    form = HomeRoomForm(request.POST or None, instance=instance)
    if request.method == "POST" and form.is_valid():
        _room = form.save(commit=False)
        if _room.config:
            _room = set_room_config(_room)

        _room.name = instance.homeroom.name
        _room.save()
        if instance.owner == request.user or request.user.is_superuser:
            messages.success(request, _("Home room was updated successfully."))
        else:
            messages.error(request, _("Only owner can update their own home rooms!"))
            logger.warning("{} has tried to update home room of {}".format(request.user, instance.owner))
        return redirect('home')
    return render(request, 'home_rooms_update.html', {'form': form})


@login_required
def personal_room_create(request):
    if request.method == "POST":
        form = PersonalRoomForm(request.POST or None, co_qs=True)
    else:
        form = PersonalRoomForm(request.POST or None, co_qs=False)
    if request.method == "POST" and form.is_valid():
        _personal_room: PersonalRoom = form.save(commit=False)
        # Custom form validations
        _personal_room.owner = request.user
        _personal_room.tenant = GeneralParameter.load().personal_rooms_tenant
        if _personal_room.config:
            _personal_room = set_room_config(_personal_room)

        _personal_room.save()
        _personal_room.co_owners.clear()
        for u in form.cleaned_data.get('co_owners'):
            _personal_room.co_owners.add(u)
        messages.success(request, _("Personal room {} was created successfully.").format(_personal_room.name))
        # return to URL
        return redirect('home')
    return render(request, 'personal_rooms_create.html', {'form': form})


@login_required
def personal_room_update(request, personal_room):
    instance = get_object_or_404(PersonalRoom, pk=personal_room)
    if request.user == instance.owner:
        if "co_owners" in request.POST:
            # setattr(form.fields['co_owners'], 'queryset', User.objects.filter(pk__in=request.POST['co_owners']))
            form = PersonalRoomForm(request.POST or None, instance=instance, co_qs=True)
        else:
            form = PersonalRoomForm(request.POST or None, instance=instance, co_qs=False)
    else:
        form = PersonalRoomCoOwnerForm(request.POST or None, instance=instance)
    if request.method == "POST":

        if form.is_valid():
            _room = form.save(commit=False)

            if request.user != instance.personalroom.owner:
                _room.name = instance.personalroom.name
                _room.owner = instance.personalroom.owner
                _room.is_public = instance.personalroom.is_public
                _room.comment_public = instance.personalroom.comment_public

            if _room.config:
                _room.mute_on_start = _room.config.mute_on_start
                _room.all_moderator = _room.config.all_moderator
                _room.everyone_can_start = _room.config.everyone_can_start
                _room.authenticated_user_can_start = _room.config.authenticated_user_can_start
                _room.guest_policy = _room.config.guest_policy
                _room.allow_guest_entry = _room.config.allow_guest_entry
                _room.access_code = _room.config.access_code
                _room.access_code_guests = _room.config.access_code_guests
                _room.disable_cam = _room.config.disable_cam
                _room.disable_mic = _room.config.disable_mic
                _room.allow_recording = _room.config.allow_recording
                _room.url = _room.config.url
                _room.maxParticipants = _room.config.maxParticipants

            if request.user != instance.personalroom.owner:
                _room.name = instance.personalroom.name
                _room.owner = instance.personalroom.owner
                _room.is_public = instance.personalroom.is_public
                _room.comment_public = instance.personalroom.comment_public
            else:
                _room.co_owners.clear()
                for u in form.cleaned_data.get('co_owners'):
                    _room.co_owners.add(u)

            _room.save()
            messages.success(request, _("Personal room {} was updated successfully.").format(_room.name))
            return redirect('home')
    return render(request, 'personal_rooms_create.html', {'form': form})


@login_required
def personal_room_delete(request, personal_room):
    instance: PersonalRoom = get_object_or_404(PersonalRoom, pk=personal_room)
    if instance.owner == request.user or request.user.is_superuser:
        instance.delete()
        messages.success(request, _("Personal room was deleted successfully."))
    else:
        messages.error(request, _("Only owner can delete a personal room!"))
        logger.warning("{} has tried to delete room {}".format(request.user, instance.owner))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


def room_config_overview(request):
    context = {
        "configs": RoomConfiguration.objects.all()
    }
    return render(request, "room_configs_overview.html", context)


@login_required
@staff_member_required
def room_config_create(request):
    form = RoomConfigurationForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        room_config = form.save(commit=False)

        # Custom form validations
        if "CustomConfig" in request.user.groups.all():
            room_config.user = request.user
            room_config.name = "{}-{}".format(request.user.username, room_config.name)
        room_config.save()

        messages.success(request, _("Room configuration was created successfully."))
        # return to URL
        return redirect('room_configs_overview')
    return render(request, 'room_configs_create.html', {'form': form})


@login_required
@staff_member_required
def room_config_delete(request, room_config):
    instance = get_object_or_404(RoomConfiguration, pk=room_config)
    instance.delete()
    messages.success(request, _("Room configuration was deleted successfully."))
    return redirect('room_configs_overview')


@login_required
@staff_member_required
def room_config_update(request, room_config):
    instance = get_object_or_404(RoomConfiguration, pk=room_config)
    form = RoomConfigurationForm(request.POST or None, instance=instance)
    if request.method == "POST" and form.is_valid():
        _room_config = form.save(commit=False)
        _room_config.save()
        _room_config.room_set.filter(state=ROOM_STATE_INACTIVE).update(
            mute_on_start=_room_config.mute_on_start,
            all_moderator=_room_config.all_moderator,
            everyone_can_start=_room_config.everyone_can_start,
            authenticated_user_can_start=_room_config.authenticated_user_can_start,
            guest_policy=_room_config.guest_policy,
            allow_guest_entry=_room_config.allow_guest_entry,
            access_code=_room_config.access_code,
            access_code_guests=_room_config.access_code_guests,
            disable_cam=_room_config.disable_cam,
            disable_mic=_room_config.disable_mic,
            allow_recording=_room_config.allow_recording,
            url=_room_config.url,
            welcome_message=_room_config.welcome_message,
            logoutUrl=_room_config.logoutUrl,
            dialNumber=_room_config.dialNumber,
            maxParticipants=_room_config.maxParticipants,
        )
        messages.success(request, _("Room configuration was updated successfully."))
        return redirect('room_configs_overview')
    return render(request, 'room_configs_create.html', {'form': form})


@login_required
@staff_member_required
def tenant_overview(request):
    f = TenantFilter(request.GET, queryset=Tenant.objects.all())
    f.form.helper = TenantFilterFormHelper
    return render(request, "tenants_overview.html", context={"filter": f})


@login_required
@staff_member_required
def tenant_create(request):
    form = TenantForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        tenant = form.save(commit=False)

        # Custom form validations

        tenant.save()

        messages.success(request, _("Tenant was created successfully."))
        # return to URL
        return redirect('tenants_overview')
    return render(request, 'tenants_create.html', {'form': form})


@login_required
@staff_member_required
def tenant_delete(request, tenant):
    instance = get_object_or_404(Tenant, pk=tenant)
    instance.delete()
    messages.success(request, _("Tenant was deleted successfully."))
    return redirect('tenants_overview')


@login_required
@staff_member_required
def tenant_update(request, tenant):
    instance = get_object_or_404(Tenant, pk=tenant)
    form = TenantForm(request.POST or None, instance=instance)
    if request.method == "POST" and form.is_valid():
        _tenant = form.save(commit=False)

        _tenant.save()

        messages.success(request, _("Tenant was updated successfully."))
        return redirect('tenants_overview')
    return render(request, 'tenants_create.html', {'form': form})


def session_set_username(request):
    if request.method != "POST":
        return HttpResponseNotAllowed(['POST'])
    request.session['username'] = request.POST.get("username")
    return HttpResponse('ok')


def session_set_username_pre_join(request, meeting_id):
    if "username" in request.session:
        return redirect('join_or_create_meeting', meeting_id)
    return render(request, 'set_username.html', {})


def join_or_create_meeting(request, meeting_id):
    _room = get_object_or_404(Room, meeting_id=meeting_id)
    context = {}
    context.update({
        "room": _room
    })
    if not request.user.is_authenticated and "username" not in request.session:
        return redirect('set_username_pre_join', meeting_id)
    if not request.user.is_authenticated and not _room.allow_guest_entry:
        messages.warning(request,
                         _("The meeting is not allowed for guest entry. Please login to access the meeting."))
        response = redirect('login')
        response['Location'] += '?next={}'.format(request.path_info)
        return response
    if _room.state == ROOM_STATE_INACTIVE and join_or_create_meeting_permissions(request.user, _room):
        return redirect('create_meeting', _room.meeting_id)
    return redirect('join_meeting', _room.meeting_id)


@non_atomic_requests
def create_meeting(request, meeting_id):
    # Query Database Object and lock it for feature requests!
    instance = get_object_or_404(Room, meeting_id=meeting_id)
    if not request.user.is_authenticated and "username" not in request.session:
        return redirect('set_username_pre_join', meeting_id)
    if not join_or_create_meeting_permissions(request.user, instance):
        return redirect("join_meeting", instance.meeting_id)
    form = ConfigureRoomForm(request.POST or None, instance=instance)
    if "username" in request.session:
        join_name = request.session['username']
    else:
        join_name = str(request.user)
    if request.method == "POST" and form.is_valid():
        _room = form.save(commit=False)
        with atomic():
            if Room.objects.filter(meeting_id=meeting_id, state=ROOM_STATE_INACTIVE).update(state=ROOM_STATE_CREATING,
                                                                                            participant_count=1,
                                                                                            last_running=now()) >= 1:
                context = {
                    'name': instance.name,
                    'meeting_id': instance.meeting_id,
                    'attendee_pw': instance.attendee_pw,
                    'moderator_pw': instance.moderator_pw,
                    'mute_on_start': _room.mute_on_start,
                    'all_moderator': _room.all_moderator,
                    'everyone_can_start': _room.everyone_can_start,
                    'access_code': _room.access_code,
                    'access_code_guests': _room.access_code_guests,
                    'disable_cam': _room.disable_cam,
                    'disable_mic': _room.disable_mic,
                    'disable_note': _room.disable_note,
                    'disable_private_chat': _room.disable_private_chat,
                    'disable_public_chat': _room.disable_public_chat,
                    'allow_recording': _room.allow_recording,
                    'guest_policy': _room.guest_policy,
                    'url': _room.url,
                    'creator': join_name,
                    'allow_guest_entry': _room.allow_guest_entry,
                    'logoutUrl': _room.logoutUrl,
                    'dialNumber': _room.dialNumber,
                    'welcome_message': _room.welcome_message,
                    'maxParticipants': _room.maxParticipants,
                }
                meeting_create(context)
                return redirect('join_meeting', _room.meeting_id)
            else:
                messages.warning(request,
                                 _("The meeting was started by another moderator."
                                   " You will be redirected to the meeting shortly."))
                return redirect('join_or_create_meeting', instance.meeting_id)
    return render(request, 'configure_room_for_meeting.html', {'form': form, 'room': instance})


def join_meeting(request, meeting_id):  # noqa: C901 TODO
    _room = get_object_or_404(Room, meeting_id=meeting_id)
    context = {}
    context.update({
        "room": _room
    })
    # User not logged in and meeting not public redirect to frontpage with message
    if _room.allow_guest_entry and not request.user.is_authenticated:
        if "username" in request.session:
            username = request.session['username']
        else:
            return redirect('join_or_create_meeting', _room.meeting_id)
    elif not request.user.is_authenticated:
        messages.warning(request,
                         _("The moderator did not allow guest entry. Please login to access the meeting."))
        response = redirect('login')
        response['Location'] += '?next={}'.format(request.path_info)
        return response
    # Check if meeting is running
    if _room.state == ROOM_STATE_ACTIVE:
        # Set variables to start or join meeting
        join_name = str(request.user) if request.user.is_authenticated else username
        password = get_join_password(request.user, _room, join_name)
        # check if guests are allowed
        if not _room.access_code:
            if _room.allow_guest_entry and not request.user.is_authenticated:
                # Check if guest access code is set
                if not _room.access_code_guests:
                    try:
                        del request.session['username']
                    except KeyError:
                        pass
                    return redirect(create_join_meeting_url(_room.meeting_id, join_name, password))
                else:
                    if request.method == "POST" and request.GET.get('action') == "guest_access_code":
                        ac = request.POST.get('access_code')
                        if ac and ac == _room.access_code_guests:
                            try:
                                del request.session['username']
                            except KeyError:
                                pass
                            return redirect(create_join_meeting_url(_room.meeting_id, join_name, password))
                        else:
                            messages.error(request, _("Incorrect guest access code."))
                    context.update({
                        "guest_access_code": True
                    })
                    return render(request, "join_meeting.html", context)
            elif not _room.allow_guest_entry and not request.user.is_authenticated:
                messages.warning(request,
                                 _("The moderator did not allow guest entry. Please login to access the meeting."))
                response = redirect('login')
                response['Location'] += '?next={}'.format(request.path_info)
                return response
            else:
                try:
                    del request.session['username']
                except KeyError:
                    pass
                return redirect(create_join_meeting_url(_room.meeting_id, join_name, password))
        else:
            if request.method == "POST" and request.GET.get('action') == "general_access_code":
                ac = request.POST.get('access_code')
                if ac and ac == _room.access_code:
                    if not _room.allow_guest_entry and not request.user.is_authenticated:
                        messages.warning(request,
                                         _("The moderator did not allow guest entry." +
                                           " Please login to access the meeting."))
                        response = redirect('login')
                        response['Location'] += '?next={}'.format(request.path_info)
                        return response
                    try:
                        del request.session['username']
                    except KeyError:
                        pass
                    return redirect(create_join_meeting_url(_room.meeting_id, join_name, password))
                else:
                    messages.error(request, _("Incorrect access code."))
            context.update({
                "access_code": True
            })
            # Meeting is not running or access code is needed
    return render(request, "join_meeting.html", context)


def get_meeting_status(request, meeting_id):
    _room = get_object_or_404(Room, meeting_id=meeting_id)
    if _room.state == ROOM_STATE_ACTIVE:
        logger.info("Was asked for state of {}. Answer is {}".format(_room, _room.state))
        return JsonResponse({"is_running": True})
    logger.info("Was asked for state of {}. Answer is {}".format(_room, _room.state))
    return JsonResponse({"is_running": False})


@csrf_exempt
def callback_bbb(request):
    logger.info("Start processing bbb callback request...")
    logger.info(request.POST['event'])

    token = request.META.get('HTTP_AUTHORIZATION').split(" ")[1]
    j = json.loads(request.POST['event'])
    domain = request.POST['domain']
    event = j[0]['data']['id']
    i = Server.objects.get(dns=domain)
    if token == i.shared_secret:
        logger.info("Token validation passed.")

        if event in ['meeting-created', 'meeting-ended']:
            meeting_id = j[0]['data']['attributes']['meeting']['external-meeting-id']
            logger.info(
                "event {} for meeting (id={}, domain={})".format(event, meeting_id, domain))
            if event == "meeting-created":
                breakout = j[0]['data']['attributes']['meeting']['is-breakout']
                name = j[0]['data']['attributes']['meeting']['name']
                meta = translate_bbb_meta_data(j[0]['data']['attributes']['meeting']['metadata'])
                logger.info(
                    "meeting created (domain={}, id={}, room-name={})".format(domain, name,
                                                                              meeting_id))
                meta.update({"name": name, "server": i, "tenant": i.tenant, "last_running": now(),
                             "state": ROOM_STATE_ACTIVE, "is_breakout": breakout})
                if "bbb-origin" in j[0]['data']['attributes']['meeting']['metadata'].keys() and \
                        j[0]['data']['attributes']['meeting']['metadata']["bbb-origin"] == "Moodle":
                    room, created = MoodleRoom.objects.update_or_create(
                        meeting_id=meeting_id,
                        defaults=meta
                    )
                else:
                    room, created = Room.objects.update_or_create(
                        meeting_id=meeting_id,
                        defaults=meta
                    )
                if created:
                    logger.info(
                        "room created (name={}, server={}, tenant={}, breakout={}, )".format(room.name,
                                                                                             room.server.dns,
                                                                                             room.tenant.name,
                                                                                             room.is_breakout))
                else:
                    logger.info(
                        "room updated (name={}, server={}, tenant={},breakout={}, )".format(room.name,
                                                                                            room.server.dns,
                                                                                            room.tenant.name,
                                                                                            room.is_breakout))
            if event == "meeting-ended":
                room = Room.objects.get(meeting_id=meeting_id)
                logger.info(
                    "meeting ended (domain={}, id={}, room-name={})".format(domain, room.name,
                                                                            room.meeting_id))
                if room.is_breakout or room.is_moodleroom():
                    meeting_type = "breakout" if room.is_breakout else "moodle created and"
                    logger.info(
                        "meeting is {} will be deleted(domain={}, id={}, room-name={})".format(meeting_type, domain,
                                                                                               room.meeting_id,
                                                                                               room.name,
                                                                                               ))
                    room.delete()
                else:
                    logger.info(
                        "meeting is not breakout will be reset(domain={}, id={}, room-name={})".format(domain,
                                                                                                       room.meeting_id,
                                                                                                       room.name))
                    reset_room(room.meeting_id, room.name, room.config)

    logger.info("Finished processing bbb callback request")
    return JsonResponse({"status": "ok"})


@csrf_exempt
def recording_callback(request):
    data = json.loads(request.body)
    if data['token'] == settings.RECORDINGS_SECRET:
        try:
            Meeting.objects.filter(pk=data['rooms_meeting_id']).update(replay_id=data['bbb_meeting_id'])
            return HttpResponse(status=200)
        except Meeting.DoesNotExist:
            return HttpResponse(status=400)
    else:
        return HttpResponse(status=401)


@csrf_exempt
def api_server_registration(request):
    data = json.loads(request.body)
    try:
        t = Tenant.objects.get(token_registration=data['token'])
        Server.objects.get_or_create(dns=data['server'], shared_secret=data['secret'], tenant=t)
        return HttpResponse(status=200)
    except Tenant.DoesNotExist:
        return HttpResponse(status=401)


@login_required
def recordings_list(request):
    context = {}
    if request.user.is_staff:
        context.update({"recordings": Meeting.objects.filter(replay_id__isnull=False)})
    else:
        context.update({"recordings": Meeting.objects.filter(
            creator=str(request.user),
            replay_id__isnull=False)})
    context.update({"base_url": settings.BASE_URL})
    return render(request, "recordings_overview.html", context)


def recording_redirect(request, replay_id):
    if request.user.is_authenticated:
        meeting = get_object_or_404(Meeting, replay_id=replay_id)
        gp = GeneralParameter.load()
        return redirect(gp.playback_url + meeting.replay_id)
    else:
        messages.warning(request,
                         _("The meeting is not allowed for guest entry. Please login to access the meeting."))
        response = redirect('login')
        response['Location'] += '?next={}'.format(request.path_info)
        return response


@login_required
@staff_member_required
def force_end_meeting(request, room_pk):
    room = get_object_or_404(Room, pk=room_pk)
    if room.end_meeting():
        messages.success(request, _("Meeting in {} successfully ended.").format(room.name))
    else:
        messages.error(request, _("Error while trying to end meeting in {}.").format(room.name))
    return redirect('rooms_overview')


@login_required
@staff_member_required
def users_overview(request):
    f = UserFilter(request.GET, queryset=User.objects.all())
    f.form.helper = TenantFilterFormHelper
    return render(request, 'users_overview.html', {'filter': f})


@login_required
@staff_member_required
def user_create(request):
    form = UserForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        user = form.save(commit=False)
        user.set_password(user.password)
        # Custom form validations

        user.save()
        for group in form.cleaned_data.get('groups'):
            user.groups.add(group)
        messages.success(request, _("User {} successfully created.").format(user.username))
        # return to URL
        return redirect('users_overview')
    return render(request, 'user_create.html', {'form': form})


@login_required
@staff_member_required
def user_delete(request, user):
    instance = get_object_or_404(User, pk=user)
    instance.delete()
    messages.success(request, _("User successfully deleted."))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required
@staff_member_required
def user_update(request, user):
    instance = get_object_or_404(User, pk=user)

    if request.method == "POST":
        form = UserUpdateForm(request.POST, instance=instance)
        if form.is_valid():
            _user = form.save(commit=False)

            _user.save()
            _user.groups.clear()
            for group in form.cleaned_data.get('groups'):
                _user.groups.add(group)

            messages.success(request, _("User {} successfully updated").format(_user.username))
            return redirect('users_overview')
    form = UserUpdateForm(instance=instance, initial={'groups': instance.groups.all()})
    return render(request, 'user_create.html', {'form': form})


@login_required
@user_passes_test(lambda user: user.has_usable_password())
def change_password(request):
    form = PasswordChangeCrispyForm(request.user, request.POST or None)
    if request.method == "POST" and form.is_valid():
        user = form.save()
        update_session_auth_hash(request, user)
        messages.success(request, _("Password successfully changed."))
        return redirect('home')

    return render(request, 'password_change.html', {'form': form})


@login_required
def change_theme(request: HttpRequest) -> HttpResponse:
    if request.method == "GET":
        return render(request, "change_theme.html", {"themes": Theme.objects.all().order_by("name")})
    elif request.method == "POST":
        if "theme" in request.POST:
            theme = request.POST["theme"]
            try:
                request.user.theme = Theme.objects.get(pk=theme)
                request.user.save()
            except Theme.DoesNotExist:
                pass
        return redirect("change_theme")
    else:
        return HttpResponseNotAllowed(["GET", "POST"])


def bbb_initialization_request(request, tenant_name):
    tenant = Tenant.objects.get(name=tenant_name)
    params = request.GET.copy()
    if validate_bbb_api_call("", params, tenant):
        return HttpResponse("""
        <response>
            <returncode>SUCCESS</returncode>
            <version>0.9</version>
        </response>""", content_type='text/xml')


@csrf_exempt
def bbb_api_create(request, tenant_name):
    # Create Meeting and return XML to moodle
    tenant = Tenant.objects.get(name=tenant_name)
    params = request.GET.copy()
    if validate_bbb_api_call("create", params, tenant):
        try:
            room, created = moodle_get_or_create_room_with_params(params, tenant)
        except IntegrityError:
            room = Room.objects.get(meeting_id=params['meetingID'])
        response = meeting_create(params, room).text
        return HttpResponse(response, content_type='text/xml')
    return bbb_checksum_error_xml_response()


def bbb_api_join(request, tenant_name):
    # Create Join URL and return to Moodle
    tenant = Tenant.objects.get(name=tenant_name)
    params = request.GET.copy()
    if validate_bbb_api_call("join", params, tenant):
        try:
            room = Room.objects.get(meeting_id=params['meetingID'], tenant=tenant)
            bbb = BigBlueButton(room.server.dns, room.server.shared_secret)
            return redirect(bbb.join(params))
        except Room.DoesNotExist:
            logger.error("Moodle room does not exist in database with ID: {}".format(params['meetingID']))
            return bbb_meeting_not_found()
    return bbb_checksum_error_xml_response()


def bbb_api_end(request, tenant_name):
    # End Meeting
    tenant = Tenant.objects.get(name=tenant_name)
    params = request.GET.copy()
    if validate_bbb_api_call("end", params, tenant):
        try:
            room = Room.objects.get(meeting_id=params['meetingID'], tenant=tenant)
            bbb = BigBlueButton(room.server.dns, room.server.shared_secret)
            response = bbb.end(room.meeting_id, room.moderator_pw)
            return HttpResponse(response.text, content_type='text/xml')
        except Room.DoesNotExist:
            logger.error("Moodle room does not exist in database with ID: {}".format(params['meetingID']))
            return bbb_meeting_not_found()
    return bbb_checksum_error_xml_response()


def bbb_api_is_meeting_running(request, tenant_name):
    # Get Status of Meeting and return XML
    tenant = Tenant.objects.get(name=tenant_name)
    params = request.GET.copy()
    if validate_bbb_api_call("isMeetingRunning", params, tenant):
        try:
            room = Room.objects.get(meeting_id=params['meetingID'], tenant=tenant)
            bbb = BigBlueButton(room.server.dns, room.server.shared_secret)
            response = bbb.is_meeting_running(room.meeting_id)
            return HttpResponse(response.text, content_type='text/xml')
        except Room.DoesNotExist or AttributeError:
            logger.error("Moodle room does not exist in database with ID: {}".format(params['meetingID']))
            return bbb_meeting_not_found()
    return bbb_checksum_error_xml_response()


def bbb_api_get_meeting_info(request, tenant_name):
    # Get Meeting Infos and return XML
    tenant = Tenant.objects.get(name=tenant_name)
    params = request.GET.copy()
    if validate_bbb_api_call("getMeetingInfo", params, tenant):
        try:
            room = Room.objects.get(meeting_id=params['meetingID'], tenant=tenant)
            bbb = BigBlueButton(room.server.dns, room.server.shared_secret)
            response = bbb.get_meeting_infos(room.meeting_id)
            return HttpResponse(response.text, content_type='text/xml')
        except Room.DoesNotExist or AttributeError:
            logger.error("Moodle room does not exist in database with ID: {}".format(params['meetingID']))
            return bbb_meeting_not_found()
    return bbb_checksum_error_xml_response()


def bbb_api_get_meetings(request, tenant_name):
    # Get all Running meeting for Tenant and return XML
    pass


def bbb_api_get_recordings(request, tenant_name):
    tenant = Tenant.objects.get(name=tenant_name)
    params = request.GET.copy()
    if validate_bbb_api_call("getRecordings", params, tenant):
        servers = Server.objects.filter(tenant=tenant, server_types__in=[ServerType.objects.get(name="playback").pk])
        recording_response = parse_recordings_from_multiple_sources(servers, params)
        if recording_response:
            return HttpResponse(parse_dict_to_xml(recording_response), content_type='text/xml')
    return bbb_no_recordings_meeting()


@login_required
def json_search_user(request):
    q = request.GET.get("q")
    users = []
    if q and len(q) >= 2:
        for user in User.objects.filter(Q(first_name__icontains=q) | Q(last_name__icontains=q) | Q(email__icontains=q)):
            users.append({"id": user.id, "display_name": str(user)})
    return JsonResponse({"users": users})
