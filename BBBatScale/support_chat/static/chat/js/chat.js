/**
 * @typedef SupporterOptions
 * @type {object}
 * @property {string} chatOwner
 * @property {string} chatOwnerRealName
 * @property {Support} support
 */

/**
 * Create a new chat card and initialize the connection to the server.
 *
 * @class
 * @requires createWebSocket
 * @requires formatDate
 * @requires updateUnreadMessagesBadge
 * @requires SortedList
 * @param {string} basePath The base path to connect the WebSockets to
 * (excluding the 'chat'/'chat/<username>' path element).
 * See [createWebSocket]{@link createWebSocket} for more information.
 * @param {?SupporterOptions} [supporterOptions=null]
 * @return {Chat}
 */
function Chat(basePath, supporterOptions = null) {
    if (!(this instanceof Chat)) {
        return new Chat(basePath, supporterOptions);
    }

    /**
     * @type {Chat}
     */
    const self = this;

    /**
     * @type {HTMLDivElement}
     */
    let chatCard;
    /**
     * @type {?HTMLElement}
     */
    let supportStatusBadge = null;
    /**
     * @type {?HTMLButtonElement}
     */
    let leaveChatButton = null;
    /**
     * @type {HTMLSpanElement}
     */
    let unreadMessagesBadge;
    /**
     * @type {HTMLDivElement}
     */
    let messagesScrollContainer;
    /**
     * @type {HTMLDivElement}
     */
    let historyLoadingSpinner;
    /**
     * @type {HTMLDivElement}
     */
    let messagesContainer;
    /**
     * @type {HTMLDivElement}
     */
    let newMessagesLoadingSpinner;
    /**
     * @type {HTMLTextAreaElement}
     */
    let messageInput;
    /**
     * @type {HTMLDivElement}
     */
    let messageInputContainer;
    /**
     * @type {HTMLSpanElement}
     */
    let charsLeftLabel;
    /**
     * @type {HTMLButtonElement}
     */
    let sendButton;
    /**
     * @type {?HTMLButtonElement}
     */
    let joinChatButton = null;

    /**
     * @type {?WebSocket}
     */
    let webSocket = null;

    /**
     * @type {number}
     */
    let scrollAnimated = 0;

    /**
     * @type {number}
     */
    let scrollTop = 0;

    /**
     * @type {?boolean}
     */
    let hasJoinedChat = supporterOptions ? false : null;
    /**
     * @type {boolean}
     */
    let loadingHistory = false;
    /**
     * @type {boolean}
     */
    let loadingNewMessages = false;

    /**
     * @typedef Message
     * @type {object}
     * @property {number} id
     * @property {Date} timestamp
     * @property {HTMLDivElement} messageContainer
     */

    /**
     * @type {SortedList<Date, Message>}
     */
    const messages = new SortedList(
        /**
         * @param {Message} lhs
         * @param {Date|Message} rhs
         * @return {number}
         */
        function (lhs, rhs) {
            if (!(rhs instanceof Date)) {
                rhs = rhs.timestamp;
            }
            return lhs.timestamp.getTime() - rhs.getTime();
        }, function (lhs, rhs) {
            return lhs.id === rhs.id;
        }
    );
    /**
     * @type {number}
     */
    let unreadMessagesCount = 0;
    /**
     * @type {boolean}
     */
    let isHistoryFullyLoaded = false;
    /**
     * Indicates whether the chat card is expanded/open or not.
     *
     * @type {boolean}
     */
    let isChatCardOpen = false;

    /**
     * @type {boolean}
     */
    let isSupportOnline = false;
    /**
     * @type {boolean}
     */
    let hasGainedSupport = false;

    /**
     * @typedef MessageAlignment
     * @type {Object}
     * @property {string} messageContainer
     * @property {string} messageAuthor
     * @property {string} messageTimestamp
     */

    /**
     * @type {MessageAlignment}
     */
    const MESSAGE_LEFT = {
        messageContainer: 'left',
        messageAuthor: 'float-left',
        messageTimestamp: 'float-right'
    };
    /**
     * @type {MessageAlignment}
     */
    const MESSAGE_RIGHT = {
        messageContainer: 'right',
        messageAuthor: 'float-right',
        messageTimestamp: 'float-left'
    };

    /**
     * @return {boolean} True if the message container is scrolled down to its end (10px tolerance), false otherwise.
     */
    function isMessagesContainerScrolledToEnd() {
        return (scrollTop + messagesScrollContainer.clientHeight + newMessagesLoadingSpinner.clientHeight + 10)
            >= messagesScrollContainer.scrollHeight;
    }

    /**
     * Scrolls down the [messagesScrollContainer]{@link messagesScrollContainer} to 'top'
     * and sets [scrollTop]{@link scrollTop} respectively or if 'top' is not supplied,
     * scrolls to [scrollTop]{@link scrollTop}.
     *
     * @param {boolean} animated Whether or not to animate the scrolling.
     * @param {?number} [top=null] The y coordinate in pixels from the top of the
     * [messagesScrollContainer]{@link messagesScrollContainer} to scroll to.
     */
    function scroll(animated, top = null) {
        if (top !== null) {
            scrollTop = top;
        }

        if (animated) {
            scrollAnimated++;
            jQuery(messagesScrollContainer).animate({'scrollTop': scrollTop}, 'slow', 'swing', function () {
                scrollAnimated--;
            });
        } else {
            jQuery(messagesScrollContainer).scrollTop(scrollTop);
        }
    }

    /**
     * Create a new message container, which will display the message as well as the user name and a timestamp
     * and add it to the [messagesContainer]{@link messagesContainer}.
     *
     * @param {number} id
     * @param {boolean} isOwnMessage
     * @param {string} userRealName
     * @param {Date} timestamp
     * @param {string} message
     */
    function addMessageContainer(id, isOwnMessage, userRealName, timestamp, message) {
        let alignment;
        if (isOwnMessage) {
            alignment = MESSAGE_RIGHT;
        } else {
            alignment = MESSAGE_LEFT;
        }

        const messageContainer = document.createElement('div');
        messageContainer.style.display = 'none';
        messageContainer.classList.add('direct-chat-msg', alignment.messageContainer);

        {
            const messageInfoContainer = document.createElement('div');
            messageInfoContainer.classList.add('direct-chat-infos', 'clearfix');
            messageContainer.appendChild(messageInfoContainer);

            {
                const messageAuthor = document.createElement('span');
                messageAuthor.classList.add('direct-chat-name', alignment.messageAuthor);
                messageAuthor.textContent = userRealName;
                messageInfoContainer.appendChild(messageAuthor);

                const messageTimestamp = document.createElement('span');
                messageTimestamp.classList.add('direct-chat-timestamp', alignment.messageTimestamp);
                messageTimestamp.textContent = formatDate(timestamp);
                messageInfoContainer.appendChild(messageTimestamp);
            }

            const messageContent = document.createElement('span');
            messageContent.classList.add('direct-chat-text');
            messageContent.textContent = message;
            messageContainer.appendChild(messageContent);
        }

        const jQueryMessagesContainer = jQuery(messagesContainer);
        const scrolledToEnd = isMessagesContainerScrolledToEnd();

        const index = messages.ensureExists({'id': id, 'timestamp': timestamp, 'messageContainer': messageContainer});
        if (index !== null) {
            const followingMessage = messages.get(index + 1);
            if (followingMessage) {
                jQuery(followingMessage.messageContainer).before(messageContainer);
            } else {
                jQueryMessagesContainer.append(messageContainer);
            }

            const animate = index !== 0 && followingMessage !== null;

            if (animate) {
                messageContainer.style.display = 'block';
            } else {
                jQuery(messageContainer).fadeIn('slow');
            }

            if (scrolledToEnd) {
                scrollTop = messagesScrollContainer.scrollHeight;
                sendMessagesReadIfEligible();
            } else {
                const jQueryMessageContainer = jQuery(messageContainer);
                if (index === 0 || jQueryMessageContainer.position().top <= 0) {
                    scrollTop += jQueryMessageContainer.outerHeight(true);
                }
            }

            scroll(animate);
        }
    }

    /**
     * Updates the [character counter]{@link charsLeftLabel} how many characters are left till 250
     * and updates the [send button]{@link sendButton}.
     * - If the count is less then 25 and greater or equal to 0, the background changes to 'bg-warning'.
     * - If the count is less then 0, the background changes to 'bg-danger'
     *   and the [send button]{@link sendButton} will be disabled.
     * - If [hasJoinedChat]{@link hasJoinedChat} is false, the [input]{@link messageInput} is empty,
     *   or the [webSocket]{@link webSocket} is not [open]{@link Chat.isOpen},
     *   the [send button]{@link sendButton} will be disabled.
     */
    function updateMessageInput() {
        let sendButtonDisabled = false;

        const charsLeft = 250 - messageInput.value.length;
        charsLeftLabel.textContent = charsLeft.toString();
        if (charsLeft > 25) {
            charsLeftLabel.classList.remove('bg-warning', 'bg-danger');
        } else if (charsLeft >= 0) {
            charsLeftLabel.classList.remove('bg-danger');
            charsLeftLabel.classList.add('bg-warning');
        } else {
            charsLeftLabel.classList.remove('bg-warning');
            charsLeftLabel.classList.add('bg-danger');
            sendButtonDisabled = true;
        }

        const message = messageInput.value;
        if (hasJoinedChat === false || !message || /^\s*$/.test(message) || !self.isOpen()) {
            sendButtonDisabled = true;
        }

        sendButton.disabled = sendButtonDisabled;
    }

    /**
     * Load the history of the chat if not already completely loaded.
     */
    function loadHistory() {
        if (!isHistoryFullyLoaded && !loadingHistory && !loadingNewMessages) {
            loadingHistory = true;
            historyLoadingSpinner.classList.add('show');

            const data = {
                'type': 'getHistory'
            };
            const message = messages.get(0);
            if (message) {
                data['beforeMessageId'] = message.id;
            }
            webSocket.send(JSON.stringify(data));
        }
    }

    /**
     * Loads the history, but only if the [messagesScrollContainer]{@link messagesScrollContainer}
     * contains not enough elements to be scrollable.
     * This function is needed in order to load the history until enough history is available
     * to trigger the scroll event if needed.
     */
    function loadHistoryIfNotScrollable() {
        if (messagesScrollContainer.clientHeight > 0
            && messagesContainer.clientHeight <= messagesScrollContainer.clientHeight) {
            historyLoadingSpinner.classList.remove('fade');
            loadHistory();
            scroll(false, scrollTop + historyLoadingSpinner.clientHeight);
        }
    }

    /**
     * Loads the new messages of the chat after the chat has been (re)opened.
     */
    function loadNewMessages() {
        if (!loadingNewMessages) {
            const message = messages.get(-1);
            if (message) {
                const scrolledToEnd = isMessagesContainerScrolledToEnd();
                loadingNewMessages = true;
                newMessagesLoadingSpinner.classList.add('show');
                if (scrolledToEnd) {
                    self.scrollDown();
                }

                webSocket.send(JSON.stringify({
                    'type': 'getNewMessages',
                    'afterMessageId': message.id
                }));
            }
        }
    }

    /**
     * Sends the 'messagesRead' command, but only if the chat card is open,
     * the [messagesScrollContainer]{@link messagesScrollContainer} is scrolled down to the end,
     * the document has focus and if [hasJoinedChat]{@link hasJoinedChat} is not false.
     */
    function sendMessagesReadIfEligible() {
        if (webSocket !== null && isChatCardOpen && hasJoinedChat !== false && document.hasFocus()
            && isMessagesContainerScrolledToEnd()) {
            function sendMessagesRead() {
                webSocket.send(JSON.stringify({
                    'type': 'messagesRead'
                }));
            }

            if (webSocket.readyState === WebSocket.OPEN) {
                sendMessagesRead();
            } else {
                addListener(webSocket, 'onopen', sendMessagesRead);
            }
        }
    }

    /**
     * Update the [joinChatButton]{@link joinChatButton}, [leaveChatButton]{@link leaveChatButton},
     * [messageInputContainer]{@link messageInputContainer}, and [hasJoinedChat]{@link hasJoinedChat}
     * to indicate that the supporter has joined the chat and now can actively support the user.
     * This will also call [updateMessageInput]{@link updateMessageInput}.
     */
    function joinedChat() {
        if (hasJoinedChat !== null && joinChatButton && leaveChatButton) {
            hasJoinedChat = true;

            joinChatButton.classList.add('d-none');
            joinChatButton.disabled = true;

            leaveChatButton.classList.remove('d-none');
            leaveChatButton.disabled = false;

            messageInputContainer.classList.remove('d-none');

            updateMessageInput();
            sendMessagesReadIfEligible();

            messageInput.focus();
        }
    }

    /**
     * Sends the 'joinChat' command to indicate that the supporter is now actively supporting the chat.
     * This command is only applicable if this chat runs as supporter, otherwise this function does nothing.
     */
    function joinChat() {
        if (supporterOptions && supporterOptions.support.getStatus() === 'active' && self.isOpen()) {
            webSocket.send(JSON.stringify({
                'type': 'joinChat'
            }));
            joinedChat();
        }
    }

    /**
     * Update the [joinChatButton]{@link joinChatButton}, [leaveChatButton]{@link leaveChatButton},
     * [messageInputContainer]{@link messageInputContainer}, and [hasJoinedChat]{@link hasJoinedChat}
     * to indicate that the supporter has left the chat and for further actions first must join it again.
     * This will also call [updateMessageInput]{@link updateMessageInput}.
     */
    function leftChat() {
        if (hasJoinedChat !== null && joinChatButton && leaveChatButton) {
            hasJoinedChat = false;

            joinChatButton.classList.remove('d-none');
            joinChatButton.disabled = supporterOptions.support.getStatus() === 'inactive';

            leaveChatButton.classList.add('d-none');
            leaveChatButton.disabled = true;

            messageInputContainer.classList.add('d-none');

            updateMessageInput();
        }
    }

    /**
     * If [isSupportOnline]{@link isSupportOnline} is false,
     * changes the [supportStatusBadge]{@link supportStatusBadge} to red in order to indicate
     * that the support is offline.
     *
     * If [isSupportOnline]{@link isSupportOnline} is true and the [hasGainedSupport]{@link hasGainedSupport} is false,
     * changes the [supportStatusBadge]{@link supportStatusBadge} to yellow in order to indicate
     * that no Supporter is currently supporting the chat,
     * but at least one Supporter is online.
     *
     * If [isSupportOnline]{@link isSupportOnline} is true and the [hasGainedSupport]{@link hasGainedSupport} is true,
     * changes the [supportStatusBadge]{@link supportStatusBadge} to green in order to indicate
     * that at least one Supporter has entered the chat.
     */
    function updateSupportStatusBadge() {
        if (supportStatusBadge) {
            if (!isSupportOnline) {
                supportStatusBadge.style.color = '#FF0000';
                supportStatusBadge.title = gettext('Support offline');
            } else if (!hasGainedSupport) {
                supportStatusBadge.style.color = '#FFAF00';
                supportStatusBadge.title = gettext('Support online');
            } else {
                supportStatusBadge.style.color = '#00FF00';
                supportStatusBadge.title = gettext('Supporter joined');
            }
        }
    }

    /**
     * Creates a div element representing a loading spinner.
     * The spinner will initially be hidden.
     *
     * @return {HTMLDivElement} A new loading spinner.
     */
    function createLoadingSpinner() {
        const loadingSpinner = document.createElement('div');
        loadingSpinner.classList.add('loading-spinner');

        {
            const loadingSpinnerCircle = document.createElement('i');
            loadingSpinnerCircle.classList.add('circle', 'fas', 'fa-spinner', 'fa-spin');
            loadingSpinnerCircle.setAttribute('role', 'status');
            loadingSpinner.appendChild(loadingSpinnerCircle);

            const loadingSpinnerSR = document.createElement('span');
            loadingSpinnerSR.classList.add('sr-only');
            loadingSpinnerSR.textContent = 'Loading...';
            loadingSpinner.appendChild(loadingSpinnerSR);
        }
        return loadingSpinner;
    }

    /**
     * Changes the [supportStatusBadge]{@link supportStatusBadge} to yellow in order to indicate
     * that no Supporter is currently supporting the chat,
     * but at least one Supporter is online.
     */
    function supportOnline() {
        isSupportOnline = true;
        updateSupportStatusBadge();
    }

    /**
     * Changes the [supportStatusBadge]{@link supportStatusBadge} to red in order to indicate
     * that the support is offline.
     */
    function supportOffline() {
        isSupportOnline = false;
        updateSupportStatusBadge();
    }

    /**
     * Changes the [supportStatusBadge]{@link supportStatusBadge} to green in order to indicate
     * that at least one Supporter has entered the chat.
     */
    function gainedSupport() {
        hasGainedSupport = true;
        updateSupportStatusBadge();
    }

    /**
     * Changes the [supportStatusBadge]{@link supportStatusBadge} to yellow in order to indicate
     * that no Supporter is currently supporting the chat
     * if at least one Supporter is online, red otherwise.
     */
    function lostSupport() {
        hasGainedSupport = false;
        updateSupportStatusBadge();
    }

    /**
     * Create the chat card.
     */
    {
        chatCard = document.createElement('div');
        chatCard.classList.add('card', 'direct-chat', 'direct-chat-primary', 'm-0');
        if (supporterOptions) {
            chatCard.classList.add('tab-pane', 'shadow-none');
            chatCard.setAttribute('role', 'tabpanel');
        } else {
            chatCard.classList.add('card-primary', 'collapsed-card');
        }

        {
            const chatCardHeader = document.createElement('div');
            chatCardHeader.classList.add('card-header', 'uniform');
            if (!supporterOptions) {
                chatCardHeader.dataset.cardWidget = 'collapse';
            }
            chatCard.appendChild(chatCardHeader);

            {
                if (supporterOptions) {
                    const showChatOverviewButton = document.createElement('button');
                    showChatOverviewButton.classList.add('tab-overview-toggle', 'mr-2', 'btn', 'btn-secondary');
                    showChatOverviewButton.addEventListener('click', function () {
                        supporterOptions.support.showChatOverview();
                    });
                    chatCardHeader.appendChild(showChatOverviewButton);

                    {
                        const showChatOverviewButtonIcon = document.createElement('i');
                        showChatOverviewButtonIcon.classList.add('fas', 'fa-bars');
                        showChatOverviewButton.appendChild(showChatOverviewButtonIcon);
                    }
                }

                const chatCardHeaderTitle = document.createElement('h3');
                chatCardHeaderTitle.classList.add('card-title');
                if (supporterOptions) {
                    chatCardHeaderTitle.textContent = supporterOptions.chatOwnerRealName;
                } else {
                    chatCardHeaderTitle.textContent = gettext('Support');
                }
                chatCardHeader.appendChild(chatCardHeaderTitle);

                const chatCardHeaderTools = document.createElement('div');
                chatCardHeaderTools.classList.add('card-tools', 'ml-auto', 'spaced');
                chatCardHeader.appendChild(chatCardHeaderTools);

                {
                    if (!supporterOptions) {
                        const supportStatusBadgeSpan = document.createElement('span');
                        supportStatusBadgeSpan.classList.add('badge', 'p-0');
                        chatCardHeaderTools.appendChild(supportStatusBadgeSpan);

                        {
                            supportStatusBadge = document.createElement('i');
                            supportStatusBadge.classList.add('fas', 'fa-circle');
                            supportStatusBadgeSpan.appendChild(supportStatusBadge);
                            updateSupportStatusBadge();
                        }
                    }

                    unreadMessagesBadge = document.createElement('span');
                    unreadMessagesBadge.classList.add('badge', 'badge-secondary');
                    chatCardHeaderTools.appendChild(unreadMessagesBadge);

                    if (supporterOptions) {
                        leaveChatButton = document.createElement('button');
                        leaveChatButton.classList.add('btn', 'btn-danger');
                        leaveChatButton.textContent = gettext('Leave Chat');
                        leaveChatButton.addEventListener('click', function () {
                            self.leaveChat();
                        });
                        chatCardHeaderTools.appendChild(leaveChatButton);
                    } else {
                        const chatCardHeaderCollapseButton = document.createElement('button');
                        chatCardHeaderCollapseButton.classList.add('btn', 'btn-tool', 'p-0');
                        chatCardHeaderTools.appendChild(chatCardHeaderCollapseButton);

                        {
                            const chatCardHeaderCollapseButtonIcon = document.createElement('i');
                            chatCardHeaderCollapseButtonIcon.classList.add('fas', 'fa-plus');
                            chatCardHeaderCollapseButton.appendChild(chatCardHeaderCollapseButtonIcon);
                        }
                    }
                }
            }

            const chatCardBody = document.createElement('div');
            chatCardBody.classList.add('card-body');
            chatCard.appendChild(chatCardBody);

            {
                messagesScrollContainer = document.createElement('div');
                messagesScrollContainer.classList.add('direct-chat-messages');
                if (supporterOptions) {
                    messagesScrollContainer.classList.add('h-100');
                }
                chatCardBody.appendChild(messagesScrollContainer);

                {
                    historyLoadingSpinner = createLoadingSpinner();
                    messagesScrollContainer.appendChild(historyLoadingSpinner);

                    messagesContainer = document.createElement('div');
                    messagesScrollContainer.appendChild(messagesContainer);

                    newMessagesLoadingSpinner = createLoadingSpinner();
                    newMessagesLoadingSpinner.classList.add('fade');
                    messagesScrollContainer.appendChild(newMessagesLoadingSpinner);
                }
            }

            const chatCardFooter = document.createElement('div');
            chatCardFooter.classList.add('card-footer');
            if (supporterOptions) {
                chatCardFooter.classList.add('fix-height');
            }
            chatCard.appendChild(chatCardFooter);

            {
                messageInputContainer = document.createElement('div');
                messageInputContainer.classList.add('input-group');
                chatCardFooter.appendChild(messageInputContainer);

                {
                    const chatCardFooterInputGroupPrepend = document.createElement('span');
                    chatCardFooterInputGroupPrepend.classList.add('input-group-prepend');
                    messageInputContainer.appendChild(chatCardFooterInputGroupPrepend);

                    {
                        charsLeftLabel = document.createElement('span');
                        charsLeftLabel.classList.add('input-group-text');
                        chatCardFooterInputGroupPrepend.appendChild(charsLeftLabel);
                    }

                    messageInput = document.createElement('textarea');
                    messageInput.classList.add('form-control', 'message-input');
                    messageInput.placeholder = gettext('Message...');
                    messageInputContainer.appendChild(messageInput);

                    const chatCardFooterInputGroupAppend = document.createElement('span');
                    chatCardFooterInputGroupAppend.classList.add('input-group-append');
                    messageInputContainer.appendChild(chatCardFooterInputGroupAppend);

                    {
                        sendButton = document.createElement('button');
                        sendButton.classList.add('btn', 'btn-primary', 'w-100');
                        sendButton.textContent = gettext('Send');
                        chatCardFooterInputGroupAppend.appendChild(sendButton);
                    }

                    updateMessageInput();
                }

                if (supporterOptions) {
                    joinChatButton = document.createElement('button');
                    joinChatButton.classList.add('btn', 'btn-success', 'w-100');
                    joinChatButton.textContent = gettext('Join Chat');
                    joinChatButton.addEventListener('click', joinChat);
                    chatCardFooter.appendChild(joinChatButton);

                    leftChat();
                }
            }
        }
    }

    /**
     * Add event listeners.
     */
    {
        document.addEventListener('focus', function () {
            sendMessagesReadIfEligible();
        });

        messagesScrollContainer.addEventListener('scroll', function () {
            if (scrollAnimated === 0) {
                scrollTop = messagesScrollContainer.scrollTop;
            }
            if (scrollTop <= historyLoadingSpinner.clientHeight) {
                historyLoadingSpinner.classList.add('fade');
                loadHistory();
            }
            sendMessagesReadIfEligible();
        });

        messageInput.addEventListener('keydown', function (event) {
            if (event.key === 'Enter' && (event.altKey || event.shiftKey)) {
                event.preventDefault();
                sendButton.click();
            }
        });

        messageInput.addEventListener('input', updateMessageInput);

        sendButton.addEventListener('click', function () {
            const message = messageInput.value;
            webSocket.send(JSON.stringify({
                'type': 'send',
                'message': message
            }));
            messageInput.value = '';
            updateMessageInput();
        });

        const jQueryChatCard = jQuery(chatCard);
        if (supporterOptions) {
            supporterOptions.support.addSupporterStatusChangedListener(function (status) {
                if (status === 'inactive') {
                    leftChat();
                } else {
                    joinChatButton.disabled = joinChatButton.classList.contains('d-none');
                }
            });

            jQueryChatCard.on('shown.supportchat.chat', function () {
                if (self.isOpen()) {
                    loadHistoryIfNotScrollable();
                }
                messageInput.focus();

                isChatCardOpen = true;

                sendMessagesReadIfEligible();
            })
            jQueryChatCard.on('hidden.supportchat.chat', function () {
                isChatCardOpen = false;
            })
        } else {
            jQueryChatCard.on('expanded.lte.cardwidget', function () {
                if (self.isOpen()) {
                    loadHistoryIfNotScrollable();
                }
                isChatCardOpen = true;
                messageInput.focus();
            })
            jQueryChatCard.on('collapsed.lte.cardwidget', function () {
                isChatCardOpen = false;
            })
        }
    }

    /**
     * Return the chat card html element.
     *
     * @return {HTMLDivElement}
     */
    this.getCard = function () {
        return chatCard;
    };

    /**
     * Scrolls down the message container animated.
     */
    this.scrollDown = function () {
        scroll(true, messagesScrollContainer.scrollHeight);
    }

    /**
     * Focuses the message input.
     */
    this.focusMessageInput = function () {
        messageInput.focus();
    }

    /**
     * @return {number} The unread messages count.
     */
    this.getUnreadMessagesCount = function () {
        return unreadMessagesCount;
    };

    /**
     * Set the number of the unread messages and also update the badge.
     *
     * @param {number} count
     */
    this.setUnreadMessagesCount = function (count) {
        unreadMessagesCount = count;
        updateUnreadMessagesBadge(jQuery(unreadMessagesBadge), count);
    };

    /**
     * Sends the 'leaveChat' command to indicate that the supporter is no longer supporting the chat.
     * This command is only applicable if this chat runs as supporter, otherwise this function does nothing.
     */
    this.leaveChat = function () {
        if (supporterOptions && supporterOptions.support.getStatus() === 'active' && this.isOpen()) {
            webSocket.send(JSON.stringify({
                'type': 'leaveChat'
            }));
            leftChat();
        }
    }

    /**
     * @return {boolean} True if a Supporter joined the chat, false otherwise.
     */
    this.hasGainedSupport = function () {
        return hasGainedSupport;
    };

    /**
     * Open the web socket for the chat.
     */
    this.open = function () {
        if (webSocket === null) {
            let url = getUrl(basePath, 'chat');
            if (supporterOptions) {
                url = getUrl(url, supporterOptions.chatOwner);
            }
            webSocket = createWebSocket(url);

            webSocket.onmessage = function (event) {
                const data = JSON.parse(event.data);
                if (!data.hasOwnProperty('type')) {
                    console.error('Received a malformed request:', JSON.stringify(data));
                    return;
                }
                const type = data['type'];
                switch (type) {
                    case 'error': {
                        const code = data['code'];
                        if (code === 6) {
                            jQuery(document).Toasts('create', {
                                'title': 'Message too long',
                                'body': 'The message you\'ve sent is too long.',
                                'autohide': true,
                                'class': 'bg-warning',
                                'delay': 5000
                            });
                        }
                        console.error('An unexpected error occurred [code:', code + ']:', data['message']);
                        break;
                    }
                    case 'history': {
                        loadingHistory = false;
                        historyLoadingSpinner.classList.remove('show');

                        for (const message of data['messages']) {
                            addMessageContainer(message['id'], message['isOwnMessage'], message['userRealName'],
                                new Date(message['timestamp']), message['message']);
                        }

                        loadHistoryIfNotScrollable();
                        break;
                    }
                    case 'historyEnd': {
                        loadingHistory = false;
                        isHistoryFullyLoaded = true;
                        historyLoadingSpinner.classList.add('fade');
                        historyLoadingSpinner.classList.remove('show');
                        break;
                    }
                    case 'newMessages': {
                        loadingNewMessages = false;
                        newMessagesLoadingSpinner.classList.remove('show');

                        for (const message of data['messages']) {
                            addMessageContainer(message['id'], message['isOwnMessage'], message['userRealName'],
                                new Date(message['timestamp']), message['message']);
                        }

                        loadHistoryIfNotScrollable();
                        break;
                    }
                    case 'tooManyNewMessages': {
                        loadingNewMessages = false;
                        newMessagesLoadingSpinner.classList.remove('show');

                        jQuery(messagesContainer).empty();

                        isHistoryFullyLoaded = false;
                        messages.clear();

                        loadHistoryIfNotScrollable();
                        break;
                    }
                    case 'message': {
                        addMessageContainer(data['id'], data['isOwnMessage'], data['userRealName'],
                            new Date(data['timestamp']), data['message']);
                        break;
                    }
                    case 'unreadMessagesCount': {
                        self.setUnreadMessagesCount(data['unreadMessagesCount']);
                        break;
                    }
                    case 'supportOffline': {
                        supportOffline();
                        break;
                    }
                    case 'supportOnline': {
                        supportOnline();
                        break;
                    }
                    case 'chatGainedSupport': {
                        gainedSupport();
                        break;
                    }
                    case 'chatLostSupport': {
                        lostSupport();
                        break;
                    }
                    default: {
                        console.error('Received an unexpected type:', type);
                        break;
                    }
                }
            };

            webSocket.onopen = function () {
                updateMessageInput();
                loadNewMessages();
                loadHistoryIfNotScrollable();

                webSocket.send(JSON.stringify({
                    'type': 'getUnreadMessagesCount'
                }));
                webSocket.send(JSON.stringify({
                    'type': 'getSupportStatus'
                }));
                webSocket.send(JSON.stringify({
                    'type': 'getChatStatus'
                }));
            };

            webSocket.onclose = function (error) {
                if (error.code !== 1000 && error.code !== 1001) {
                    console.error('Chat socket closed unexpectedly: ' + error.code);
                }
                self.close();
            };
        }
    };

    /**
     * @return {boolean} True if the chat connection to the server is open and ready, otherwise false.
     */
    this.isOpen = function () {
        return webSocket !== null && webSocket.readyState === WebSocket.OPEN;
    };

    /**
     * Closes the chat connection to the server.
     */
    this.close = function () {
        if (webSocket !== null) {
            webSocket.close(1000);
            webSocket = null;

            leftChat();
        }
    };
}
