from django.apps import AppConfig


class SupportChatConfig(AppConfig):
    name = 'support_chat'
