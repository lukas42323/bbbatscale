from __future__ import annotations

from enum import Enum, unique, auto
from typing import Optional, List, Tuple, Any, Dict

from asgiref.sync import sync_to_async
from channels.db import database_sync_to_async
from django.db import transaction

from core.constants import WEBHOOK_SUPPORT_CHAT_INCOMING_MESSAGE
from core.models import User
from core.webhooks import webhook_enqueue
from support_chat.models import ChatMessage, Chat, Supporter
from support_chat.utils import has_supporter_privileges
from utils.websockets import WebsocketConsumer, websocket_consumer_decorator, Error


@unique
class ChatUserType(Enum):
    OWNER = auto()
    SUPPORTER = auto()


class SupportStatusListener:
    GROUP = 'support_status'

    async def support_status_changed(self, support_online: bool) -> None:
        pass


class SupporterStatusListener:
    GROUP = 'supporter_status'

    async def supporter_status_changed(self, is_active: bool, username: str) -> None:
        pass


@websocket_consumer_decorator
class ChatConsumer(SupportStatusListener, SupporterStatusListener, WebsocketConsumer):

    def __init__(self, scope: dict, user: User, username: str = None) -> None:
        super().__init__(scope, user)
        if username:
            self.is_supporter_joined = False
            self.chat_owner_username = username
        else:
            self.is_supporter_joined = None
            self.chat_owner_username = None  # user.username is not ready for use at this point

        # can not add self.chat_group to self.groups yet, since self.chat_owner_username may has not been initialized
        self.groups = [SupportStatusListener.GROUP, SupporterStatusListener.GROUP]

    @property
    def is_supporter(self) -> bool:
        return isinstance(self.is_supporter_joined, bool)

    @property
    def group(self) -> str:
        if self.chat_owner_username is None:
            raise ValueError('self.chat_owner_username has not been set yet')
        return f'chat_{self.chat_owner_username}'

    async def test_permissions(self) -> Optional[Error]:
        if self.is_supporter and not (
                self.user.is_active
                and self.user.is_authenticated
                and await database_sync_to_async(has_supporter_privileges)(self.user)
        ):
            return Error.STAFF_REQUIRED
        return None

    async def test_chat_exists(self) -> Optional[Error]:
        if self.is_supporter and not await database_sync_to_async(
                lambda: Chat.objects.filter(id__username=self.chat_owner_username).exists())():
            return Error.CHAT_NOT_FOUND
        return None

    async def test_is_active_supporter(self) -> Optional[Error]:
        if not self.is_supporter:
            return Error.PERMISSION_DENIED
        else:
            supporter = await database_sync_to_async(Supporter.get)(user=self.user)
            if not supporter or not supporter.is_active:
                return Error.SUPPORTER_STATUS_INACTIVE
        return None

    async def test_is_user_or_supporter_joined(self) -> Optional[Error]:
        if self.is_supporter_joined is False:
            return Error.SUPPORTER_NOT_JOINED_CHAT
        return None

    @WebsocketConsumer.login_required(accept=True, close=True)
    @WebsocketConsumer.passes_test(lambda self: self.test_permissions(), close=True)
    @WebsocketConsumer.passes_test(lambda self: self.test_chat_exists(), close=True)
    async def connect(self) -> None:
        if not self.is_supporter:
            self.chat_owner_username = self.user.username

        self.groups.append(self.group)
        await self.channel_layer.group_add(self.group, self.channel_name)
        # add self.chat_group to self.groups and call group_add, since self.chat_owner_username is now initialized

    @WebsocketConsumer.if_opened(raise_exception=True)
    async def receive_json(self, content: dict, **kwargs: Any) -> None:  # noqa: C901
        if 'type' not in content:
            await self.send_json(Error.MALFORMED_REQUEST)
            return

        content_type = content['type']
        if content_type == 'getHistory':
            if 'beforeMessageId' in content and not isinstance(content['beforeMessageId'], int):
                await self.send_json(Error.MALFORMED_REQUEST)
                return
            await self.command_get_history(content.get('beforeMessageId', None))
        elif content_type == 'getNewMessages':
            if 'afterMessageId' not in content or not isinstance(content['afterMessageId'], int):
                await self.send_json(Error.MALFORMED_REQUEST)
                return
            await self.command_get_new_messages(content['afterMessageId'])
        elif content_type == 'send':
            if 'message' not in content or not isinstance(content['message'], str):
                await self.send_json(Error.MALFORMED_REQUEST)
                return
            await self.command_send(content['message'])
        elif content_type == 'messagesRead':
            await self.command_messages_read()
        elif content_type == 'getUnreadMessagesCount':
            await self.command_get_unread_messages_count()
        elif content_type == 'getChatStatus':
            await self.command_get_chat_status()
        elif content_type == 'getSupportStatus':
            await self.command_get_support_status()
        elif content_type == 'joinChat':
            await self.command_join_chat()
        elif content_type == 'leaveChat':
            await self.command_leave_chat()
        else:
            await self.send_json(Error.UNKNOWN_REQUEST)

    async def command_get_history(self, before_message_id: Optional[int]) -> None:
        chat = await database_sync_to_async(Chat.get)(owner=self.chat_owner_username)
        if chat is not None:
            @transaction.atomic
            def get_history() -> List[Dict[str, Any]]:
                if isinstance(before_message_id, int):
                    messages_query = ChatMessage.objects.filter(chat=chat, id__lt=before_message_id)
                else:
                    messages_query = ChatMessage.objects.filter(chat=chat)
                messages_query = messages_query.order_by('-id')
                return list(
                    map(lambda chat_message: self._inflate_chat_message_dict(_chat_message_to_dict(chat_message)),
                        messages_query[:10]))

            messages = await database_sync_to_async(get_history)()
            if messages:
                await self.send_json({
                    'type': 'history',
                    'messages': messages
                })
            else:
                await self.send_json({
                    'type': 'historyEnd',
                })
        else:
            await self.send_json({
                'type': 'historyEnd',
            })

    async def command_get_new_messages(self, after_message_id: int) -> None:
        chat = await database_sync_to_async(Chat.get)(owner=self.chat_owner_username)
        if chat is not None:
            @transaction.atomic
            def get_messages() -> Optional[List[Dict[str, Any]]]:
                messages_query = ChatMessage.objects.filter(chat=chat, id__gt=after_message_id).order_by('id')
                if messages_query.count() > 10:
                    return None
                return list(
                    map(lambda chat_message: self._inflate_chat_message_dict(_chat_message_to_dict(chat_message)),
                        messages_query))

            messages = await database_sync_to_async(get_messages)()
            if messages is not None:
                await self.send_json({
                    'type': 'newMessages',
                    'messages': messages
                })
            else:
                await self.send_json({
                    'type': 'tooManyNewMessages'
                })
        else:
            await self.send_json({
                'type': 'newMessages',
                'messages': []
            })

    @WebsocketConsumer.passes_test(lambda self: self.test_is_user_or_supporter_joined())
    async def command_send(self, message: str) -> None:
        if len(message.encode('utf-16-le')) / 2 > 250:
            await self.send_json(Error.MESSAGE_TOO_LONG)
            return

        @transaction.atomic
        def create_chat_message() -> Tuple[Chat, bool, User, ChatMessage]:
            _chat, _created = Chat.get_or_create(owner=self.chat_owner_username)
            _chat_message = ChatMessage(chat=_chat, user=self.user, message=message)
            _chat_message.save(is_supporter=self.is_supporter)
            return _chat, _created, _chat.owner, _chat_message

        chat, created, owner, chat_message = await database_sync_to_async(create_chat_message)()

        await sync_to_async(webhook_enqueue)(
            WEBHOOK_SUPPORT_CHAT_INCOMING_MESSAGE,
            _prepare_webhook_payload(chat_message))

        if created:
            await self.group_send_json(SupportConsumer.GROUP, SupportConsumer.send_chat, owner.username,
                                       str(owner), chat.unread_owner_messages,
                                       chat.is_support_active, chat_message.message,
                                       chat_message.timestamp.isoformat())

        await self.group_send_json(self.group, self.send_chat_message_dict,
                                   chat_message=_chat_message_to_dict(chat_message))
        await self.group_send_json(SupportConsumer.GROUP, SupportConsumer.new_message,
                                   chat_owner=self.chat_owner_username, message=chat_message.message,
                                   timestamp=chat_message.timestamp.isoformat())

        if self.is_supporter:
            await self.group_send_json(self.group, self.unread_messages_count)
        else:
            await self.group_send_json(SupportConsumer.GROUP, SupportConsumer.unread_messages_count,
                                       chat_owner=self.chat_owner_username)

    @WebsocketConsumer.passes_test(lambda self: self.test_is_user_or_supporter_joined())
    async def command_messages_read(self) -> None:
        @transaction.atomic
        def messages_read() -> bool:
            chat = Chat.get(owner=self.chat_owner_username)
            if chat is not None:
                if self.is_supporter and chat.unread_owner_messages != 0:
                    chat.unread_owner_messages = 0
                elif not self.is_supporter and chat.unread_support_messages != 0:
                    chat.unread_support_messages = 0
                else:
                    return False
                chat.save()
                return True
            return False

        if await database_sync_to_async(messages_read)():
            if self.is_supporter:
                await self.group_send_json(SupportConsumer.GROUP, SupportConsumer.unread_messages_count,
                                           chat_owner=self.chat_owner_username)
            else:
                await self.group_send_json(self.group, self.unread_messages_count)

    async def command_get_unread_messages_count(self) -> None:
        await self.unread_messages_count()

    async def command_get_chat_status(self) -> None:
        chat = await database_sync_to_async(Chat.get)(owner=self.chat_owner_username)
        if chat is not None:
            if chat.is_support_active:
                await self.chat_gained_support()
            else:
                await self.chat_lost_support()

    async def command_get_support_status(self) -> None:
        await self.support_status_changed(await database_sync_to_async(Supporter.has_active)())

    @WebsocketConsumer.passes_test(lambda self: self.test_is_active_supporter())
    async def command_join_chat(self) -> None:
        await self._supporter_joined_chat()

    @WebsocketConsumer.passes_test(lambda self: self.test_is_active_supporter())
    async def command_leave_chat(self) -> None:
        await self._supporter_left_chat()

    async def support_status_changed(self, support_online: bool) -> None:
        if support_online:
            await self.send_json({
                'type': 'supportOnline'
            })
        else:
            await self.send_json({
                'type': 'supportOffline'
            })

    async def supporter_status_changed(self, is_active: bool, username: str) -> None:
        if not is_active:
            await self._supporter_left_chat()

    async def unread_messages_count(self) -> None:
        unread_messages = 0
        chat = await database_sync_to_async(Chat.get)(owner=self.chat_owner_username)
        if chat is not None:
            if self.is_supporter:
                unread_messages = chat.unread_owner_messages
            else:
                unread_messages = chat.unread_support_messages

        await self.send_json({
            'type': 'unreadMessagesCount',
            'unreadMessagesCount': unread_messages
        })

    async def chat_gained_support(self) -> None:
        await self.send_json({
            'type': 'chatGainedSupport'
        })

    async def chat_lost_support(self) -> None:
        await self.send_json({
            'type': 'chatLostSupport'
        })

    @WebsocketConsumer.if_opened(raise_exception=True)
    async def disconnect(self, code: int) -> None:
        if self.is_supporter:
            await self._supporter_left_chat()

    async def _supporter_joined_chat(self) -> None:
        if self.is_supporter_joined is False:
            self.is_supporter_joined = True

            @transaction.atomic
            def supporter_join_chat() -> bool:
                chat = Chat.get(owner=self.chat_owner_username)
                is_first_supporter = not chat.is_support_active
                chat.increment_support_active()
                chat.save()
                return is_first_supporter

            if await database_sync_to_async(supporter_join_chat)():
                await self.group_send_json(self.group, self.chat_gained_support)
                await self.group_send_json(SupportConsumer.GROUP, SupportConsumer.chat_gained_support,
                                           self.chat_owner_username)

    async def _supporter_left_chat(self) -> None:
        if self.is_supporter_joined is True:
            self.is_supporter_joined = False

            @transaction.atomic
            def supporter_leave_chat() -> bool:
                chat = Chat.get(owner=self.chat_owner_username)
                chat.decrement_support_active()
                chat.save()
                return not chat.is_support_active

            if await database_sync_to_async(supporter_leave_chat)():
                await self.group_send_json(self.group, self.chat_lost_support)
                await self.group_send_json(SupportConsumer.GROUP, SupportConsumer.chat_lost_support,
                                           self.chat_owner_username)

    async def send_chat_message_dict(self, chat_message: Dict[str, Any]) -> None:
        await self.send_json({
            'type': 'message',
            **self._inflate_chat_message_dict(chat_message)
        })

    def _inflate_chat_message_dict(self, chat_message: Dict[str, Any]) -> Dict[str, Any]:
        return {
            'id': chat_message['id'],
            'message': chat_message['message'],
            'isOwnMessage': self.user.username == chat_message['username'],
            'userRealName': chat_message['userRealName'],
            'timestamp': chat_message['timestamp']
        }


@websocket_consumer_decorator
class SupportConsumer(SupporterStatusListener, WebsocketConsumer):
    GROUP = 'support'

    def __init__(self, scope: dict, user: User) -> None:
        super().__init__(scope, user)
        self.is_active = None
        self.groups = [self.GROUP, SupporterStatusListener.GROUP]

    async def test_is_supporter(self) -> Optional[Error]:
        return None if await database_sync_to_async(has_supporter_privileges)(self.user) else Error.SUPPORTER_REQUIRED

    @WebsocketConsumer.login_required(accept=True, close=True)
    @WebsocketConsumer.passes_test(lambda self: self.test_is_supporter(), close=True)
    async def connect(self) -> None:
        self.is_active = (await database_sync_to_async(Supporter.get_or_create)(user=self.user)).is_active

    @WebsocketConsumer.if_opened(raise_exception=True)
    async def receive_json(self, content: dict, **kwargs: Any) -> None:  # noqa: C901
        if 'type' not in content:
            await self.send_json(Error.MALFORMED_REQUEST)
            return

        content_type = content['type']
        if content_type == 'getChats':
            await self.command_get_chats()
        elif content_type == 'getStatus':
            await self.command_get_status()
        elif content_type == 'setStatus':
            if 'status' not in content or not isinstance(content['status'], str):
                await self.send_json(Error.MALFORMED_REQUEST)
                return
            await self.command_set_status(content['status'])
        else:
            await self.send_json(Error.UNKNOWN_REQUEST)

    async def command_get_chats(self) -> None:
        @transaction.atomic
        def get_chats() -> List[Tuple[Chat, ChatMessage]]:
            return list((_chat, ChatMessage.objects.filter(chat=_chat).order_by('-timestamp').first())
                        for _chat in Chat.objects.prefetch_related('id').all())

        for chat, latest_chat_message in await database_sync_to_async(get_chats)():
            await self.send_chat(chat.owner.username, str(chat.owner),
                                 chat.unread_owner_messages, chat.is_support_active,
                                 latest_chat_message.message,
                                 latest_chat_message.timestamp.isoformat())

    async def command_get_status(self) -> None:
        await self.supporter_status_changed(self.is_active, self.user.username, True)

    async def command_set_status(self, status: str) -> None:
        if status not in ['active', 'inactive']:
            await self.send_json(Error.MALFORMED_REQUEST)
            return

        await self.set_active(status == 'active')

    async def send_chat(self, chat_owner: str, chat_owner_real_name: str, unread_messages: int, is_support_active: bool,
                        message: str, timestamp: int) -> None:
        await self.send_json({
            'type': 'chat',
            'chatOwner': chat_owner,
            'chatOwnerRealName': chat_owner_real_name,
            'unreadMessages': unread_messages,
            'isSupportActive': is_support_active,
            'message': message,
            'timestamp': timestamp
        })

    async def new_message(self, chat_owner: str, message: str, timestamp: int) -> None:
        await self.send_json({
            'type': 'newMessage',
            'chatOwner': chat_owner,
            'message': message,
            'timestamp': timestamp
        })

    async def unread_messages_count(self, chat_owner: str) -> None:
        unread_messages = 0
        chat = await database_sync_to_async(Chat.get)(owner=chat_owner)
        if chat is not None:
            unread_messages = chat.unread_owner_messages
        await self.send_json({
            'type': 'unreadMessagesCount',
            'chatOwner': chat_owner,
            'unreadMessagesCount': unread_messages
        })

    async def chat_gained_support(self, chat_owner: str) -> None:
        await self.send_json({
            'type': 'chatGainedSupport',
            'chatOwner': chat_owner
        })

    async def chat_lost_support(self, chat_owner: str) -> None:
        await self.send_json({
            'type': 'chatLostSupport',
            'chatOwner': chat_owner
        })

    @WebsocketConsumer.if_opened(raise_exception=True)
    async def disconnect(self, code) -> None:
        pass

    async def set_active(self, active: bool) -> None:
        if self.is_active != active:
            self.is_active = active

            @transaction.atomic
            def update() -> Optional[bool]:
                old_has_active = Supporter.has_active()
                supporter = Supporter.get_or_create(self.user)
                supporter.is_active = active
                supporter.save()
                has_active = Supporter.has_active()
                if old_has_active != has_active:
                    return has_active
                return None

            support_online = await database_sync_to_async(update)()
            if support_online is not None:
                await self.group_send_json(SupportStatusListener.GROUP, SupportStatusListener.support_status_changed,
                                           support_online)

            await self.supporter_status_changed(self.is_active, self.user.username, True)

            await self.group_send_json(SupporterStatusListener.GROUP, SupporterStatusListener.supporter_status_changed,
                                       self.is_active, self.user.username)

    async def supporter_status_changed(self, is_active: bool, username: str, force: bool = False) -> None:
        if username == self.user.username:
            if force or self.is_active != is_active:
                self.is_active = is_active
                await self.send_json({
                    'type': 'status',
                    'status': 'active' if self.is_active else 'inactive',
                })


def _chat_message_to_dict(chat_message: ChatMessage) -> Dict[str, Any]:
    return {
        'id': chat_message.id,
        'message': chat_message.message,
        'username': chat_message.user.username,
        'userRealName': str(chat_message.user),
        'timestamp': chat_message.timestamp.isoformat()
    }


def _prepare_webhook_payload(chat_message: ChatMessage) -> Dict[str, Any]:
    return {
        'message': chat_message.message,
        'username': chat_message.user.username,
        'userDisplayName': str(chat_message.user),
        'timestamp': chat_message.timestamp.isoformat()
    }
